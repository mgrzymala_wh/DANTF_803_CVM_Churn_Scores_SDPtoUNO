USE [BusinessSemanticLayer]
GO


CREATE OR ALTER FUNCTION MRG.ufn_GetRModelDataFlags (@CustomerAccountIdID AS int)
RETURNS TABLE
AS
  RETURN
  (
        SELECT DISTINCT
          mrg_rmdf.CustomerAccountId,
          mrg_rmdf.CustomerAccountValTxt,
          mrg_rmdf.CustomerAccountProfileTypeCd,
          mrg_rmdf.CustomerAccountProfileMeasureCd   
        FROM MRG.RModelDataFlags mrg_rmdf WHERE mrg_rmdf.CustomerAccountId = @CustomerAccountIdID
  )
GO
