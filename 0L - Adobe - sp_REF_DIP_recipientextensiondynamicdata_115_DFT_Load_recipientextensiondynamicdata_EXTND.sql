USE [StagingAdobeOutbound]
GO

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO

-- =========================================================================================================================================================== 
-- Author:		WHGROUP\rfleming	 
-- Create date: 17/05/2017 
-- Description:	Create  [src].[sp_REF_DIP_recipientextensiondynamicdata_115_DFT_Load_recipientextensiondynamicdata]
--				 
-- ============================================= 
-- ============================================= 
-- Change History 
-- Date                 User				    Change                 
-- 17/05/2017			WHGROUP\rfleming		V1.0           First edition of comments.  NB not first edition of Proc.  
--                                                             Change from PL.aggregates to BSL.RollingAggregates and PL.Product to BSL.Product
--                                                             and from ActivityDt > to ActivityDt >= TFS bug 4638

-- 30/06/2017			Bharat Bhandari			    Added below mentioed 8 new fields as part of TFS - 4135
--												    EligibilityFlag, EligibilityDate, OptionInFlag, OptInDate, MaximumStakefactor, InPlayFlag, AccaFlag, MobileFlag

-- 12/02/2017           WHGROUP\Mspencernoble       Change to add fields to adobe TFS 7178
-- 2020-12-09           mgrzymala                   DANTF-810 - including 8 new columns for CVM and Churn Model Types and Measures
/*
-- to test:
EXEC [StagingAdobeOutbound].[SRC].[sp_REF_DIP_recipientextensiondynamicdata_115_DFT_Load_recipientextensiondynamicdata_EXTND]
*/
-- ============================================================================================================================================================ 

CREATE OR ALTER PROC [SRC].[sp_REF_DIP_recipientextensiondynamicdata_115_DFT_Load_recipientextensiondynamicdata_EXTND]
AS
    WITH    AdobeAdditionalDays
              AS ( SELECT   a.CustomerAccountId ,
                            LastGamesBetDt ,
                            LastBingoBetDt ,
                            LastBingoSideGamesBetDt ,
                            LastCasinoBetDt ,
                            LastLiveCasinoBetDt ,
                            LastMacauBetDt ,
                            LastPokerBetDt ,
                            LastPokerSideGamesBetDt ,
                            LastScratchCardsBetDt ,
                            LastSkillBetDt ,
                            LastSportsBetDt ,
                            LastVegasbetDt ,
                            GamesTot1DayBet ,
                            GamesCount1DayBet ,
                            Games1DayWin ,
                            Games1DayBonus ,
                            Games1DayActiveDays ,
                            BingoTot1DayBet ,
                            BingoCount1DayBet ,
                            Bingo1DayWin ,
                            Bingo1DayBonus ,
                            Bingo1DayActiveDays ,
                            BingoSideGamesTot1DayBet ,
                            BingoSideGamesCount1DayBet ,
                            BingoSideGames1DayWin ,
                            BingoSideGames1DayBonus ,
                            BingoSideGames1DayActiveDays ,
                            CasinoTot1DayBet ,
                            CasinoCount1DayBet ,
                            Casino1DayWin ,
                            Casino1DayBonus ,
                            Casino1DayActiveDays ,
                            LiveCasinoTot1DayBet ,
                            LiveCasinoCount1DayBet ,
                            LiveCasino1DayWin ,
                            LiveCasino1DayBonus ,
                            LiveCasino1DayActiveDays ,
                            MacauTot1DayBet ,
                            MacauCount1DayBet ,
                            Macau1DayWin ,
                            Macau1DayBonus ,
                            Macau1DayActiveDays ,
                            PokerTot1DayRake ,
                            PokerCount1DayBet ,
                            Poker1DayWin ,
                            Poker1DayBonus ,
                            Poker1DayActiveDays ,
                            PokerSideGamesTot1DayBet ,
                            PokerSideGamesCount1DayBet ,
                            PokerSideGames1DayWin ,
                            PokerSideGames1DayBonus ,
                            PokerSideGames1DayActiveDays ,
                            ScratchCardsTot1DayBet ,
                            ScratchCardsCount1DayBet ,
                            ScratchCards1DayWin ,
                            ScratchCards1DayBonus ,
                            ScratchCards1DayActiveDays ,
                            SkillTot1DayBet ,
                            SkillCount1DayBet ,
                            Skill1DayWin ,
                            Skill1DayBonus ,
                            Skill1DayActiveDays ,
                            SportsTot1DayBet ,
                            SportsCount1DayBet ,
                            Sports1DayWin ,
                            Sports1DayBonus ,
                            Sports1DayActiveDays ,
                            VegasTot1DayBet ,
                            VegasCount1DayBet ,
                            Vegas1DayWin ,
                            Vegas1DayBonus ,
                            Vegas1DayActiveDays ,
                            GamesTot30DayBet ,
                            GamesCount30DayBet ,
                            Games30DayWin ,
                            Games30DayBonus ,
                            Games30DayActiveDays ,
                            BingoTot30DayBet ,
                            BingoCount30DayBet ,
                            Bingo30DayWin ,
                            Bingo30DayBonus ,
                            Bingo30DayActiveDays ,
                            BingoSideGamesTot30DayBet ,
                            BingoSideGamesCount30DayBet ,
                            BingoSideGames30DayWin ,
                            BingoSideGames30DayBonus ,
                            BingoSideGames30DayActiveDays ,
                            CasinoTot30DayBet ,
                            CasinoCount30DayBet ,
                            Casino30DayWin ,
                            Casino30DayBonus ,
                            Casino30DayActiveDays ,
                            LiveCasinoTot30DayBet ,
                            LiveCasinoCount30DayBet ,
                            LiveCasino30DayWin ,
                            LiveCasino30DayBonus ,
                            LiveCasino30DayActiveDays ,
                            MacauTot30DayBet ,
                            MacauCount30DayBet ,
                            Macau30DayWin ,
                            Macau30DayBonus ,
                            Macau30DayActiveDays ,
                            PokerTot30DayRake ,
                            PokerCount30DayBet ,
                            Poker30DayWin ,
                            Poker30DayBonus ,
                            Poker30DayActiveDays ,
                            PokerSideGamesTot30DayBet ,
                            PokerSideGamesCount30DayBet ,
                            PokerSideGames30DayWin ,
                            PokerSideGames30DayBonus ,
                            PokerSideGames30DayActiveDays ,
                            ScratchCardsTot30DayBet ,
                            ScratchCardsCount30DayBet ,
                            ScratchCards30DayWin ,
                            ScratchCards30DayBonus ,
                            ScratchCards30DayActiveDays ,
                            SkillTot30DayBet ,
                            SkillCount30DayBet ,
                            Skill30DayWin ,
                            Skill30DayBonus ,
                            Skill30DayActiveDays ,
                            SportsTot30DayBet ,
                            SportsCount30DayBet ,
                            Sports30DayWin ,
                            Sports30DayBonus ,
                            Sports30DayActiveDays ,
                            VegasTot30DayBet ,
                            VegasCount30DayBet ,
                            Vegas30DayWin ,
                            Vegas30DayBonus ,
                            Vegas30DayActiveDays ,
                            GamesTot60DayBet ,
                            GamesCount60DayBet ,
                            Games60DayWin ,
                            Games60DayBonus ,
                            Games60DayActiveDays ,
                            BingoTot60DayBet ,
                            BingoCount60DayBet ,
                            Bingo60DayWin ,
                            Bingo60DayBonus ,
                            Bingo60DayActiveDays ,
                            BingoSideGamesTot60DayBet ,
                            BingoSideGamesCount60DayBet ,
                            BingoSideGames60DayWin ,
                            BingoSideGames60DayBonus ,
                            BingoSideGames60DayActiveDays ,
                            CasinoTot60DayBet ,
                            CasinoCount60DayBet ,
                            Casino60DayWin ,
                            Casino60DayBonus ,
                            Casino60DayActiveDays ,
                            LiveCasinoTot60DayBet ,
                            LiveCasinoCount60DayBet ,
                            LiveCasino60DayWin ,
                            LiveCasino60DayBonus ,
                            LiveCasino60DayActiveDays ,
                            MacauTot60DayBet ,
                            MacauCount60DayBet ,
                            Macau60DayWin ,
                            Macau60DayBonus ,
                            Macau60DayActiveDays ,
                            PokerTot60DayRake ,
                            PokerCount60DayBet ,
                            Poker60DayWin ,
                            Poker60DayBonus ,
                            Poker60DayActiveDays ,
                            PokerSideGamesTot60DayBet ,
                            PokerSideGamesCount60DayBet ,
                            PokerSideGames60DayWin ,
                            PokerSideGames60DayBonus ,
                            PokerSideGames60DayActiveDays ,
                            ScratchCardsTot60DayBet ,
                            ScratchCardsCount60DayBet ,
                            ScratchCards60DayWin ,
                            ScratchCards60DayBonus ,
                            ScratchCards60DayActiveDays ,
                            SkillTot60DayBet ,
                            SkillCount60DayBet ,
                            Skill60DayWin ,
                            Skill60DayBonus ,
                            Skill60DayActiveDays ,
                            SportsTot60DayBet ,
                            SportsCount60DayBet ,
                            Sports60DayWin ,
                            Sports60DayBonus ,
                            Sports60DayActiveDays ,
                            VegasTot60DayBet ,
                            VegasCount60DayBet ,
                            Vegas60DayWin ,
                            Vegas60DayBonus ,
                            Vegas60DayActiveDays ,
                            GamesTot180DayBet ,
                            GamesCount180DayBet ,
                            Games180DayWin ,
                            Games180DayBonus ,
                            Games180DayActiveDays ,
                            BingoTot180DayBet ,
                            BingoCount180DayBet ,
                            Bingo180DayWin ,
                            Bingo180DayBonus ,
                            Bingo180DayActiveDays ,
                            BingoSideGamesTot180DayBet ,
                            BingoSideGamesCount180DayBet ,
                            BingoSideGames180DayWin ,
                            BingoSideGames180DayBonus ,
                            BingoSideGames180DayActiveDays ,
                            CasinoTot180DayBet ,
                            CasinoCount180DayBet ,
                            Casino180DayWin ,
                            Casino180DayBonus ,
                            Casino180DayActiveDays ,
                            LiveCasinoTot180DayBet ,
                            LiveCasinoCount180DayBet ,
                            LiveCasino180DayWin ,
                            LiveCasino180DayBonus ,
                            LiveCasino180DayActiveDays ,
                            MacauTot180DayBet ,
                            MacauCount180DayBet ,
                            Macau180DayWin ,
                            Macau180DayBonus ,
                            Macau180DayActiveDays ,
                            PokerTot180DayRake ,
                            PokerCount180DayBet ,
                            Poker180DayWin ,
                            Poker180DayBonus ,
                            Poker180DayActiveDays ,
                            PokerSideGamesTot180DayBet ,
                            PokerSideGamesCount180DayBet ,
                            PokerSideGames180DayWin ,
                            PokerSideGames180DayBonus ,
                            PokerSideGames180DayActiveDays ,
                            ScratchCardsTot180DayBet ,
                            ScratchCardsCount180DayBet ,
                            ScratchCards180DayWin ,
                            ScratchCards180DayBonus ,
                            ScratchCards180DayActiveDays ,
                            SkillTot180DayBet ,
                            SkillCount180DayBet ,
                            Skill180DayWin ,
                            Skill180DayBonus ,
                            Skill180DayActiveDays ,
                            SportsTot180DayBet ,
                            SportsCount180DayBet ,
                            Sports180DayWin ,
                            Sports180DayBonus ,
                            Sports180DayActiveDays ,
                            VegasTot180DayBet ,
                            VegasCount180DayBet ,
                            Vegas180DayWin ,
                            Vegas180DayBonus ,
                            Vegas180DayActiveDays
                   FROM     ( SELECT    CustomerAccountId ,
                                        [Games] AS LastGamesBetDt ,
                                        [Bingo] AS LastBingoBetDt ,
                                        [Bingo Side Games] AS LastBingoSideGamesBetDt ,
                                        [Casino] AS LastCasinoBetDt ,
                                        [Live Casino] AS LastLiveCasinoBetDt ,
                                        [Macau] AS LastMacauBetDt ,
                                        [Poker] AS LastPokerBetDt ,
                                        [Poker Side Games] AS LastPokerSideGamesBetDt ,
                                        [ScratchCards] AS LastScratchCardsBetDt ,
                                        [Skill] AS LastSkillBetDt ,
                                        [Sports] AS LastSportsBetDt ,
                                        [Vegas] AS LastVegasbetDt
                              FROM      ( SELECT    CustomerAccountId ,
                                                    ProductCd ,
                                                    ActivityDt
                                          FROM      [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                    AS a
                                                    INNER JOIN BusinessSemanticLayer.[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                    AS b ON a.ProductId = b.ProductId
                                        ) P PIVOT  ( MAX([ActivityDt]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                            ) AS a
                            LEFT OUTER JOIN ( SELECT    CustomerAccountId ,
                                                        [Games] AS GamesTot30DayBet ,
                                                        [Bingo] AS BingoTot30DayBet ,
                                                        [Bingo Side Games] AS BingoSideGamesTot30DayBet ,
                                                        [Casino] AS CasinoTot30DayBet ,
                                                        [Live Casino] AS LiveCasinoTot30DayBet ,
                                                        [Macau] AS MacauTot30DayBet ,
                                                        [Poker] AS PokerTot30DayRake ,
                                                        [Poker Side Games] AS PokerSideGamesTot30DayBet ,
                                                        [ScratchCards] AS ScratchCardsTot30DayBet ,
                                                        [Skill] AS SkillTot30DayBet ,
                                                        [Sports] AS SportsTot30DayBet ,
                                                        [Vegas] AS VegasTot30DayBet
                                              FROM      ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              IIF(ProductCd = 'Poker', [SumOfFeesGBP]
                                                              + [SumOfRakesGBP], ( [SumOfBetsGBP]
                                                              - [SumOfRefundsGBP] )) AS [SumOfBetsGBP]
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -30,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfBetsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                            ) AS b ON a.CustomerAccountId = b.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS GamesCount30DayBet ,
                                                        [Bingo] AS BingoCount30DayBet ,
                                                        [Bingo Side Games] AS BingoSideGamesCount30DayBet ,
                                                        [Casino] AS CasinoCount30DayBet ,
                                                        [Live Casino] AS LiveCasinoCount30DayBet ,
                                                        [Macau] AS MacauCount30DayBet ,
                                                        [Poker] AS PokerCount30DayBet ,
                                                        [Poker Side Games] AS PokerSideGamesCount30DayBet ,
                                                        [ScratchCards] AS ScratchCardsCount30DayBet ,
                                                        [Skill] AS SkillCount30DayBet ,
                                                        [Sports] AS SportsCount30DayBet ,
                                                        [Vegas] AS VegasCount30DayBet
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              CountOfBets
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY,  -- 17/05/2017 by WHGROUP\rfleming
                                                              -30,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([CountOfBets]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS c ON a.CustomerAccountId = c.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games30DayWin ,
                                                        [Bingo] AS Bingo30DayWin ,
                                                        [Bingo Side Games] AS BingoSideGames30DayWin ,
                                                        [Casino] AS Casino30DayWin ,
                                                        [Live Casino] AS LiveCasino30DayWin ,
                                                        [Macau] AS Macau30DayWin ,
                                                        [Poker] AS Poker30DayWin ,
                                                        [Poker Side Games] AS PokerSideGames30DayWin ,
                                                        [ScratchCards] AS ScratchCards30DayWin ,
                                                        [Skill] AS Skill30DayWin ,
                                                        [Sports] AS Sports30DayWin ,
                                                        [Vegas] AS Vegas30DayWin
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              SumOfWinsGBP
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -30,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfWinsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS d ON a.CustomerAccountId = d.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games30DayBonus ,
                                                        [Bingo] AS Bingo30DayBonus ,
                                                        [Bingo Side Games] AS BingoSideGames30DayBonus ,
                                                        [Casino] AS Casino30DayBonus ,
                                                        [Live Casino] AS LiveCasino30DayBonus ,
                                                        [Macau] AS Macau30DayBonus ,
                                                        [Poker] AS Poker30DayBonus ,
                                                        [Poker Side Games] AS PokerSideGames30DayBonus ,
                                                        [ScratchCards] AS ScratchCards30DayBonus ,
                                                        [Skill] AS Skill30DayBonus ,
                                                        [Sports] AS Sports30DayBonus ,
                                                        [Vegas] AS Vegas30DayBonus
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              SumOfBonusBetsGBP
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -30,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfBonusBetsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS e ON a.CustomerAccountId = e.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games30DayActiveDays ,
                                                        [Bingo] AS Bingo30DayActiveDays ,
                                                        [Bingo Side Games] AS BingoSideGames30DayActiveDays ,
                                                        [Casino] AS Casino30DayActiveDays ,
                                                        [Live Casino] AS LiveCasino30DayActiveDays ,
                                                        [Macau] AS Macau30DayActiveDays ,
                                                        [Poker] AS Poker30DayActiveDays ,
                                                        [Poker Side Games] AS PokerSideGames30DayActiveDays ,
                                                        [ScratchCards] AS ScratchCards30DayActiveDays ,
                                                        [Skill] AS Skill30DayActiveDays ,
                                                        [Sports] AS Sports30DayActiveDays ,
                                                        [Vegas] AS Vegas30DayActiveDays
                                               FROM     ( SELECT DISTINCT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              CAST(ActivityDt AS DATE) AS ActivityDt
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -30,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( COUNT([ActivityDt]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS f ON a.CustomerAccountId = f.CustomerAccountId
                            LEFT OUTER JOIN ( SELECT    CustomerAccountId ,
                                                        [Games] AS GamesTot60DayBet ,
                                                        [Bingo] AS BingoTot60DayBet ,
                                                        [Bingo Side Games] AS BingoSideGamesTot60DayBet ,
                                                        [Casino] AS CasinoTot60DayBet ,
                                                        [Live Casino] AS LiveCasinoTot60DayBet ,
                                                        [Macau] AS MacauTot60DayBet ,
                                                        [Poker] AS PokerTot60DayRake ,
                                                        [Poker Side Games] AS PokerSideGamesTot60DayBet ,
                                                        [ScratchCards] AS ScratchCardsTot60DayBet ,
                                                        [Skill] AS SkillTot60DayBet ,
                                                        [Sports] AS SportsTot60DayBet ,
                                                        [Vegas] AS VegasTot60DayBet
                                              FROM      ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              IIF(ProductCd = 'Poker', [SumOfFeesGBP]
                                                              + [SumOfRakesGBP], ( [SumOfBetsGBP]
                                                              - [SumOfRefundsGBP] )) AS [SumOfBetsGBP]
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -60,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfBetsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                            ) AS g ON a.CustomerAccountId = g.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS GamesCount60DayBet ,
                                                        [Bingo] AS BingoCount60DayBet ,
                                                        [Bingo Side Games] AS BingoSideGamesCount60DayBet ,
                                                        [Casino] AS CasinoCount60DayBet ,
                                                        [Live Casino] AS LiveCasinoCount60DayBet ,
                                                        [Macau] AS MacauCount60DayBet ,
                                                        [Poker] AS PokerCount60DayBet ,
                                                        [Poker Side Games] AS PokerSideGamesCount60DayBet ,
                                                        [ScratchCards] AS ScratchCardsCount60DayBet ,
                                                        [Skill] AS SkillCount60DayBet ,
                                                        [Sports] AS SportsCount60DayBet ,
                                                        [Vegas] AS VegasCount60DayBet
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              CountOfBets
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -60,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([CountOfBets]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS h ON a.CustomerAccountId = h.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games60DayWin ,
                                                        [Bingo] AS Bingo60DayWin ,
                                                        [Bingo Side Games] AS BingoSideGames60DayWin ,
                                                        [Casino] AS Casino60DayWin ,
                                                        [Live Casino] AS LiveCasino60DayWin ,
                                                        [Macau] AS Macau60DayWin ,
                                                        [Poker] AS Poker60DayWin ,
                                                        [Poker Side Games] AS PokerSideGames60DayWin ,
                                                        [ScratchCards] AS ScratchCards60DayWin ,
                                                        [Skill] AS Skill60DayWin ,
                                                        [Sports] AS Sports60DayWin ,
                                                        [Vegas] AS Vegas60DayWin
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              SumOfWinsGBP
                                                          FROM 
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -60,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfWinsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS i ON a.CustomerAccountId = i.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games60DayBonus ,
                                                        [Bingo] AS Bingo60DayBonus ,
                                                        [Bingo Side Games] AS BingoSideGames60DayBonus ,
                                                        [Casino] AS Casino60DayBonus ,
                                                        [Live Casino] AS LiveCasino60DayBonus ,
                                                        [Macau] AS Macau60DayBonus ,
                                                        [Poker] AS Poker60DayBonus ,
                                                        [Poker Side Games] AS PokerSideGames60DayBonus ,
                                                        [ScratchCards] AS ScratchCards60DayBonus ,
                                                        [Skill] AS Skill60DayBonus ,
                                                        [Sports] AS Sports60DayBonus ,
                                                        [Vegas] AS Vegas60DayBonus
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              SumOfBonusBetsGBP
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -60,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfBonusBetsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS j ON a.CustomerAccountId = j.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games60DayActiveDays ,
                                                        [Bingo] AS Bingo60DayActiveDays ,
                                                        [Bingo Side Games] AS BingoSideGames60DayActiveDays ,
                                                        [Casino] AS Casino60DayActiveDays ,
                                                        [Live Casino] AS LiveCasino60DayActiveDays ,
                                                        [Macau] AS Macau60DayActiveDays ,
                                                        [Poker] AS Poker60DayActiveDays ,
                                                        [Poker Side Games] AS PokerSideGames60DayActiveDays ,
                                                        [ScratchCards] AS ScratchCards60DayActiveDays ,
                                                        [Skill] AS Skill60DayActiveDays ,
                                                        [Sports] AS Sports60DayActiveDays ,
                                                        [Vegas] AS Vegas60DayActiveDays
                                               FROM     ( SELECT DISTINCT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              CAST(ActivityDt AS DATE) AS ActivityDt
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -60,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( COUNT([ActivityDt]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS k ON a.CustomerAccountId = k.CustomerAccountId
                            LEFT OUTER JOIN ( SELECT    CustomerAccountId ,
                                                        [Games] AS GamesTot180DayBet ,
                                                        [Bingo] AS BingoTot180DayBet ,
                                                        [Bingo Side Games] AS BingoSideGamesTot180DayBet ,
                                                        [Casino] AS CasinoTot180DayBet ,
                                                        [Live Casino] AS LiveCasinoTot180DayBet ,
                                                        [Macau] AS MacauTot180DayBet ,
                                                        [Poker] AS PokerTot180DayRake ,
                                                        [Poker Side Games] AS PokerSideGamesTot180DayBet ,
                                                        [ScratchCards] AS ScratchCardsTot180DayBet ,
                                                        [Skill] AS SkillTot180DayBet ,
                                                        [Sports] AS SportsTot180DayBet ,
                                                        [Vegas] AS VegasTot180DayBet
                                              FROM      ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              IIF(ProductCd = 'Poker', [SumOfFeesGBP]
                                                              + [SumOfRakesGBP], ( [SumOfBetsGBP]
                                                              - [SumOfRefundsGBP] )) AS [SumOfBetsGBP]
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -180,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfBetsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                            ) AS l ON a.CustomerAccountId = l.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS GamesCount180DayBet ,
                                                        [Bingo] AS BingoCount180DayBet ,
                                                        [Bingo Side Games] AS BingoSideGamesCount180DayBet ,
                                                        [Casino] AS CasinoCount180DayBet ,
                                                        [Live Casino] AS LiveCasinoCount180DayBet ,
                                                        [Macau] AS MacauCount180DayBet ,
                                                        [Poker] AS PokerCount180DayBet ,
                                                        [Poker Side Games] AS PokerSideGamesCount180DayBet ,
                                                        [ScratchCards] AS ScratchCardsCount180DayBet ,
                                                        [Skill] AS SkillCount180DayBet ,
                                                        [Sports] AS SportsCount180DayBet ,
                                                        [Vegas] AS VegasCount180DayBet
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              CountOfBets
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -180,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([CountOfBets]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS m ON a.CustomerAccountId = m.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games180DayWin ,
                                                        [Bingo] AS Bingo180DayWin ,
                                                        [Bingo Side Games] AS BingoSideGames180DayWin ,
                                                        [Casino] AS Casino180DayWin ,
                                                        [Live Casino] AS LiveCasino180DayWin ,
                                                        [Macau] AS Macau180DayWin ,
                                                        [Poker] AS Poker180DayWin ,
                                                        [Poker Side Games] AS PokerSideGames180DayWin ,
                                                        [ScratchCards] AS ScratchCards180DayWin ,
                                                        [Skill] AS Skill180DayWin ,
                                                        [Sports] AS Sports180DayWin ,
                                                        [Vegas] AS Vegas180DayWin
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              SumOfWinsGBP
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -180,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfWinsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS n ON a.CustomerAccountId = n.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games180DayBonus ,
                                                        [Bingo] AS Bingo180DayBonus ,
                                                        [Bingo Side Games] AS BingoSideGames180DayBonus ,
                                                        [Casino] AS Casino180DayBonus ,
                                                        [Live Casino] AS LiveCasino180DayBonus ,
                                                        [Macau] AS Macau180DayBonus ,
                                                        [Poker] AS Poker180DayBonus ,
                                                        [Poker Side Games] AS PokerSideGames180DayBonus ,
                                                        [ScratchCards] AS ScratchCards180DayBonus ,
                                                        [Skill] AS Skill180DayBonus ,
                                                        [Sports] AS Sports180DayBonus ,
                                                        [Vegas] AS Vegas180DayBonus
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              SumOfBonusBetsGBP
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -180,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfBonusBetsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS o ON a.CustomerAccountId = o.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games180DayActiveDays ,
                                                        [Bingo] AS Bingo180DayActiveDays ,
                                                        [Bingo Side Games] AS BingoSideGames180DayActiveDays ,
                                                        [Casino] AS Casino180DayActiveDays ,
                                                        [Live Casino] AS LiveCasino180DayActiveDays ,
                                                        [Macau] AS Macau180DayActiveDays ,
                                                        [Poker] AS Poker180DayActiveDays ,
                                                        [Poker Side Games] AS PokerSideGames180DayActiveDays ,
                                                        [ScratchCards] AS ScratchCards180DayActiveDays ,
                                                        [Skill] AS Skill180DayActiveDays ,
                                                        [Sports] AS Sports180DayActiveDays ,
                                                        [Vegas] AS Vegas180DayActiveDays
                                               FROM     ( SELECT DISTINCT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              CAST(ActivityDt AS DATE) AS ActivityDt
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -180,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( COUNT([ActivityDt]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS P ON a.CustomerAccountId = P.CustomerAccountId
                            LEFT OUTER JOIN ( SELECT    CustomerAccountId ,
                                                        [Games] AS GamesTot1DayBet ,
                                                        [Bingo] AS BingoTot1DayBet ,
                                                        [Bingo Side Games] AS BingoSideGamesTot1DayBet ,
                                                        [Casino] AS CasinoTot1DayBet ,
                                                        [Live Casino] AS LiveCasinoTot1DayBet ,
                                                        [Macau] AS MacauTot1DayBet ,
                                                        [Poker] AS PokerTot1DayRake ,
                                                        [Poker Side Games] AS PokerSideGamesTot1DayBet ,
                                                        [ScratchCards] AS ScratchCardsTot1DayBet ,
                                                        [Skill] AS SkillTot1DayBet ,
                                                        [Sports] AS SportsTot1DayBet ,
                                                        [Vegas] AS VegasTot1DayBet
                                              FROM      ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              IIF(ProductCd = 'Poker', [SumOfFeesGBP]
                                                              + [SumOfRakesGBP], ( [SumOfBetsGBP]
                                                              - [SumOfRefundsGBP] )) AS [SumOfBetsGBP]
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -1,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfBetsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                            ) AS q ON a.CustomerAccountId = q.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS GamesCount1DayBet ,
                                                        [Bingo] AS BingoCount1DayBet ,
                                                        [Bingo Side Games] AS BingoSideGamesCount1DayBet ,
                                                        [Casino] AS CasinoCount1DayBet ,
                                                        [Live Casino] AS LiveCasinoCount1DayBet ,
                                                        [Macau] AS MacauCount1DayBet ,
                                                        [Poker] AS PokerCount1DayBet ,
                                                        [Poker Side Games] AS PokerSideGamesCount1DayBet ,
                                                        [ScratchCards] AS ScratchCardsCount1DayBet ,
                                                        [Skill] AS SkillCount1DayBet ,
                                                        [Sports] AS SportsCount1DayBet ,
                                                        [Vegas] AS VegasCount1DayBet
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              CountOfBets
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -1,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([CountOfBets]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS r ON a.CustomerAccountId = r.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games1DayWin ,
                                                        [Bingo] AS Bingo1DayWin ,
                                                        [Bingo Side Games] AS BingoSideGames1DayWin ,
                                                        [Casino] AS Casino1DayWin ,
                                                        [Live Casino] AS LiveCasino1DayWin ,
                                                        [Macau] AS Macau1DayWin ,
                                                        [Poker] AS Poker1DayWin ,
                                                        [Poker Side Games] AS PokerSideGames1DayWin ,
                                                        [ScratchCards] AS ScratchCards1DayWin ,
                                                        [Skill] AS Skill1DayWin ,
                                                        [Sports] AS Sports1DayWin ,
                                                        [Vegas] AS Vegas1DayWin
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              SumOfWinsGBP
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -1,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfWinsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS s ON a.CustomerAccountId = s.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games1DayBonus ,
                                                        [Bingo] AS Bingo1DayBonus ,
                                                        [Bingo Side Games] AS BingoSideGames1DayBonus ,
                                                        [Casino] AS Casino1DayBonus ,
                                                        [Live Casino] AS LiveCasino1DayBonus ,
                                                        [Macau] AS Macau1DayBonus ,
                                                        [Poker] AS Poker1DayBonus ,
                                                        [Poker Side Games] AS PokerSideGames1DayBonus ,
                                                        [ScratchCards] AS ScratchCards1DayBonus ,
                                                        [Skill] AS Skill1DayBonus ,
                                                        [Sports] AS Sports1DayBonus ,
                                                        [Vegas] AS Vegas1DayBonus
                                               FROM     ( SELECT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              SumOfBonusBetsGBP
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -1,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( SUM([SumOfBonusBetsGBP]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS t ON a.CustomerAccountId = t.CustomerAccountId
                            LEFT OUTER  JOIN ( SELECT   CustomerAccountId ,
                                                        [Games] AS Games1DayActiveDays ,
                                                        [Bingo] AS Bingo1DayActiveDays ,
                                                        [Bingo Side Games] AS BingoSideGames1DayActiveDays ,
                                                        [Casino] AS Casino1DayActiveDays ,
                                                        [Live Casino] AS LiveCasino1DayActiveDays ,
                                                        [Macau] AS Macau1DayActiveDays ,
                                                        [Poker] AS Poker1DayActiveDays ,
                                                        [Poker Side Games] AS PokerSideGames1DayActiveDays ,
                                                        [ScratchCards] AS ScratchCards1DayActiveDays ,
                                                        [Skill] AS Skill1DayActiveDays ,
                                                        [Sports] AS Sports1DayActiveDays ,
                                                        [Vegas] AS Vegas1DayActiveDays
                                               FROM     ( SELECT DISTINCT
                                                              CustomerAccountId ,
                                                              ProductCd ,
                                                              CAST(ActivityDt AS DATE) AS ActivityDt
                                                          FROM
                                                              [BusinessSemanticLayer].[MRG].[CustomerSourceBettingDailyAggregateRolling] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS a
                                                              INNER JOIN [BusinessSemanticLayer].[dbo].[Product] -- 17/05/2017 by WHGROUP\rfleming
                                                              AS b ON a.ProductId = b.ProductId
                                                          WHERE
                                                              [ActivityDt] >= DATEADD(DAY, -- 17/05/2017 by WHGROUP\rfleming
                                                              -1,
                                                              CAST(GETDATE() AS DATE))
                                                        ) P PIVOT  ( COUNT([ActivityDt]) FOR ProductCd IN ( [Bingo],
                                                              [Bingo Side Games],
                                                              [Casino],
                                                              [Live Casino],
                                                              [Games], [Macau],
                                                              [Poker],
                                                              [Poker Side Games],
                                                              [ScratchCards],
                                                              [Skill],
                                                              [Sports],
                                                              [Vegas] ) ) AS pvt
                                             ) AS u ON a.CustomerAccountId = u.CustomerAccountId
                 )
        SELECT  [global_account_id] ,
                [risk_profile_id] ,
                [viplevel_casino_id] ,
                [viplevel_poker_id] ,
                [viplevel_vegas_id] ,
                [account_balance] ,
                REDD.account_balance_gbp ,
                [last_login_date] ,
                [last_system_id] ,
                [last_withdrawal_date] ,
                [last_product_id] ,
                [last_deposit_date] ,
                [last_win_date] ,
                [last_win_amount_gbp] ,
                [last_bonus_date] ,
                [bingo_bonus_balance] ,
                [bingo_loyalty_points_balance] ,
                [bingo_pending_balance] ,
                [bingo_loyalty_level] ,
                [bingo_date_level_achieved] ,
                [bingo_current_period_loyalty_points] ,
                [activity_type_id] ,
                [activity_status_id] ,
                [total_vegas_complimentary_points] ,
                [current_balance_complimentary_points] ,
                [customer_lifestage_id] ,
                [priority_first_product_id] ,
                [priority_second_product_id] ,
                [priority_third_product_id] ,
                [priority_fourth_product_id] ,
                [priority_fifth_product_id] ,
                [priority_sixth_product_id] ,
                [priority_first_sub_product_id] ,
                [priority_second_sub_product_id] ,
                [priority_third_sub_product_id] ,
                [priority_fourth_sub_product_id] ,
                [priority_fifth_sub_product_id] ,
                [priority_sixth_sub_product_id] ,
                [poker_reactivation_status] ,
                [churn_prevention_model_flag] ,
                [player_protection_model_flag] ,
                [uvs_calculated_value] ,
                [uvs_tier_id] ,
                [PokerFirstActivityDt] ,
                [CasinoFirstActivityDt] ,
                [VegasFirstActivityDt] ,
                [GamesFirstActivityDt] ,
                [SkillFirstActivityDt] ,
                REDD.BingoSideGamesFirstActivityDt ,
                [LiveCasinoFirstActivityDt] ,
                [PokerSideGamesFirstActivityDt] ,
                [ScratchCardsGamesFirstActivityDt] ,
                [BetmostPokerFirstActivityDt] ,
                [BiGSLiCKPokerFirstActivityDt] ,
                [CDPokerFirstActivityDt] ,
                [SportsFirstActivityDt] ,
                [BingoFirstActivityDt] ,
                [tot_num_of_deposits] ,
                [total_lifetime_value_deposits_gbp] ,
                REDD.total_lifetime_value_withdrawals_gbp ,
                [total_number_of_bets] ,
                [total_lifetime_bet_value_gbp] ,
                [total_active_days] ,
                [REDD].[total_number_of_withdrawals_gbp] AS total_number_of_withdrawals ,
                [bingo_cust_acct_activity_status_id] ,
                bingosidegames_cust_acct_activity_status_id ,
                [casino_cust_acct_activity_status_id] ,
                [live_casino_cust_acct_activity_status_id] ,
                [macau_cust_acct_activity_status_id] ,
                [poker_cust_acct_activity_status_id] ,
                [scratchcard_cust_acct_activity_status_id] ,
                [sportsbook_cust_acct_activity_status_id] ,
                [skill_cust_acct_activity_status_id] ,
                [vegas_cust_acct_activity_status_id] ,
                GamesCustomerAccountActivityStatusId AS games_cust_acct_activity_status_id ,
                GamesCustomerAccountActivityStatusTypeId AS games_cust_acct_activity_type_id ,
                last_deposit_amount_gbp ,
                last_withdrawal_amount_gbp ,
                last_bonus_amount_gbp ,
                bingo_cust_acct_activity_type_id ,
                bingosidegames_cust_acct_activity_type_id ,
                live_casino_cust_acct_activity_type_id ,
                macau_cust_acct_activity_type_id ,
                MacauFirstActivityDt ,
                poker_cust_acct_activity_type_id ,
                scratchcard_cust_acct_activity_type_id ,
                skill_cust_acct_activity_type_id ,
                casino_cust_acct_activity_type_id ,
                sportsbook_cust_acct_activity_type_id ,
                vegas_cust_acct_activity_type_id ,
                REDD.DollarExchangeRate ,
                REDD.EuroExchangeRate ,
                REDD.YenExchangeRate ,
                REDD.KroneExchangeRate ,
                REDD.last_bet_date ,
                last_bet_amount_GBP ,
                LastBingoBetDt ,
                LastBingoSideGamesBetDt ,
                LastCasinoBetDt ,
                LastLiveCasinoBetDt ,
                LastMacauBetDt ,
                LastPokerBetDt ,
                LastPokerSideGamesBetDt ,
                LastScratchCardsBetDt ,
                LastSkillBetDt ,
                LastSportsBetDt ,
                LastVegasbetDt ,
                LastGamesBetDt ,
                BingoTot1DayBet AS BingoTot1DayBetGBP ,
                BingoCount1DayBet ,
                Bingo1DayWin AS Bingo1DayWinGBP ,
                Bingo1DayBonus AS Bingo1DayBonusGBP ,
                Bingo1DayActiveDays ,
                BingoSideGamesTot1DayBet AS BingoSideGamesTot1DayBetGBP ,
                BingoSideGamesCount1DayBet ,
                BingoSideGames1DayWin AS BingoSideGames1DayWinGBP ,
                BingoSideGames1DayBonus AS BingoSideGames1DayBonusGBP ,
                BingoSideGames1DayActiveDays ,
                CasinoTot1DayBet AS CasinoTot1DayBetGBP ,
                CasinoCount1DayBet ,
                Casino1DayWin AS Casino1DayWinGBP ,
                Casino1DayBonus AS Casino1DayBonusGBP ,
                Casino1DayActiveDays ,
                GamesTot1DayBet AS GamesTot1DayBetGBP ,
                GamesCount1DayBet ,
                Games1DayWin AS Games1DayWinGBP ,
                Games1DayBonus AS Games1DayBonusGBP ,
                Games1DayActiveDays ,
                LiveCasinoTot1DayBet AS LiveCasinoTot1DayBetGBP ,
                LiveCasinoCount1DayBet ,
                LiveCasino1DayWin AS LiveCasino1DayWinGBP ,
                LiveCasino1DayBonus AS LiveCasino1DayBonusGBP ,
                LiveCasino1DayActiveDays ,
                MacauTot1DayBet AS MacauTot1DayBetGBP ,
                MacauCount1DayBet ,
                Macau1DayWin AS Macau1DayWinGBP ,
                Macau1DayBonus AS Macau1DayBonusGBP ,
                Macau1DayActiveDays ,
                PokerTot1DayRake AS PokerTot1DayRakeGBP ,
                PokerCount1DayBet ,
                Poker1DayWin AS Poker1DayWinGBP ,
                Poker1DayBonus AS Poker1DayBonusGBP ,
                Poker1DayActiveDays ,
                PokerSideGamesTot1DayBet AS PokerSideGamesTot1DayBetGBP ,
                PokerSideGamesCount1DayBet ,
                PokerSideGames1DayWin AS PokerSideGames1DayWinGBP ,
                PokerSideGames1DayBonus AS PokerSideGames1DayBonusGBP ,
                PokerSideGames1DayActiveDays ,
                ScratchCardsTot1DayBet AS ScratchCardsTot1DayBetGBP ,
                ScratchCardsCount1DayBet ,
                ScratchCards1DayWin AS ScratchCards1DayWinGBP ,
                ScratchCards1DayBonus AS ScratchCards1DayBonusGBP ,
                ScratchCards1DayActiveDays ,
                SkillTot1DayBet AS SkillTot1DayBetGBP ,
                SkillCount1DayBet ,
                Skill1DayWin AS Skill1DayWinGBP ,
                Skill1DayBonus AS Skill1DayBonusGBP ,
                Skill1DayActiveDays ,
                SportsTot1DayBet AS SportsTot1DayBetGBP ,
                SportsCount1DayBet ,
                Sports1DayWin AS Sports1DayWinGBP ,
                Sports1DayBonus AS Sports1DayBonusGBP ,
                Sports1DayActiveDays ,
                VegasTot1DayBet AS VegasTot1DayBetGBP ,
                VegasCount1DayBet ,
                Vegas1DayWin AS Vegas1DayWinGBP ,
                Vegas1DayBonus AS Vegas1DayBonusGBP ,
                Vegas1DayActiveDays ,
                BingoTot30DayBet AS BingoTot30DayBetGBP ,
                BingoCount30DayBet ,
                Bingo30DayWin AS Bingo30DayWinGBP ,
                Bingo30DayBonus AS Bingo30DayBonusGBP ,
                Bingo30DayActiveDays ,
                BingoSideGamesTot30DayBet AS BingoSideGamesTot30DayBetGBP ,
                BingoSideGamesCount30DayBet ,
                BingoSideGames30DayWin AS BingoSideGames30DayWinGBP ,
                BingoSideGames30DayBonus AS BingoSideGames30DayBonusGBP ,
                BingoSideGames30DayActiveDays ,
                CasinoTot30DayBet AS CasinoTot30DayBetGBP ,
                CasinoCount30DayBet ,
                Casino30DayWin AS Casino30DayWinGBP ,
                Casino30DayBonus AS Casino30DayBonusGBP ,
                Casino30DayActiveDays ,
                GamesTot30DayBet AS GamesTot30DayBetGBP ,
                GamesCount30DayBet ,
                Games30DayWin AS Games30DayWinGBP ,
                Games30DayBonus AS Games30DayBonusGBP ,
                Games30DayActiveDays ,
                LiveCasinoTot30DayBet AS LiveCasinoTot30DayBetGBP ,
                LiveCasinoCount30DayBet ,
                LiveCasino30DayWin AS LiveCasino30DayWinGBP ,
                LiveCasino30DayBonus AS LiveCasino30DayBonusGBP ,
                LiveCasino30DayActiveDays ,
                MacauTot30DayBet AS MacauTot30DayBetGBP ,
                MacauCount30DayBet ,
                Macau30DayWin AS Macau30DayWinGBP ,
                Macau30DayBonus AS Macau30DayBonusGBP ,
                Macau30DayActiveDays ,
                PokerTot30DayRake AS PokerTot30DayRakeGBP ,
                PokerCount30DayBet ,
                Poker30DayWin AS Poker30DayWinGBP ,
                Poker30DayBonus AS Poker30DayBonusGBP ,
                Poker30DayActiveDays ,
                PokerSideGamesTot30DayBet AS PokerSideGamesTot30DayBetGBP ,
                PokerSideGamesCount30DayBet ,
                PokerSideGames30DayWin AS PokerSideGames30DayWinGBP ,
                PokerSideGames30DayBonus AS PokerSideGames30DayBonusGBP ,
                PokerSideGames30DayActiveDays ,
                ScratchCardsTot30DayBet AS ScratchCardsTot30DayBetGBP ,
                ScratchCardsCount30DayBet ,
                ScratchCards30DayWin AS ScratchCards30DayWinGBP ,
                ScratchCards30DayBonus AS ScratchCards30DayBonusGBP ,
                ScratchCards30DayActiveDays ,
                SkillTot30DayBet AS SkillTot30DayBetGBP ,
                SkillCount30DayBet ,
                Skill30DayWin AS Skill30DayWinGBP ,
                Skill30DayBonus AS Skill30DayBonusGBP ,
                Skill30DayActiveDays ,
                SportsTot30DayBet AS SportsTot30DayBetGBP ,
                SportsCount30DayBet ,
                Sports30DayWin AS Sports30DayWinGBP ,
                Sports30DayBonus AS Sports30DayBonusGBP ,
                Sports30DayActiveDays ,
                VegasTot30DayBet AS VegasTot30DayBetGBP ,
                VegasCount30DayBet ,
                Vegas30DayWin AS Vegas30DayWinGBP ,
                Vegas30DayBonus AS Vegas30DayBonusGBP ,
                Vegas30DayActiveDays ,
                BingoTot60DayBet AS BingoTot60DayBetGBP ,
                BingoCount60DayBet ,
                Bingo60DayWin AS Bingo60DayWinGBP ,
                Bingo60DayBonus AS Bingo60DayBonusGBP ,
                Bingo60DayActiveDays ,
                BingoSideGamesTot60DayBet AS BingoSideGamesTot60DayBetGBP ,
                BingoSideGamesCount60DayBet ,
                BingoSideGames60DayWin AS BingoSideGames60DayWinGBP ,
                BingoSideGames60DayBonus AS BingoSideGames60DayBonusGBP ,
                BingoSideGames60DayActiveDays ,
                CasinoTot60DayBet AS CasinoTot60DayBetGBP ,
                CasinoCount60DayBet ,
                Casino60DayWin AS Casino60DayWinGBP ,
                Casino60DayBonus AS Casino60DayBonusGBP ,
                Casino60DayActiveDays ,
                GamesTot60DayBet AS GamesTot60DayBetGBP ,
                GamesCount60DayBet ,
                Games60DayWin AS Games60DayWinGBP ,
                Games60DayBonus AS Games60DayBonusGBP ,
                Games60DayActiveDays ,
                LiveCasinoTot60DayBet AS LiveCasinoTot60DayBetGBP ,
                LiveCasinoCount60DayBet ,
                LiveCasino60DayWin AS LiveCasino60DayWinGBP ,
                LiveCasino60DayBonus AS LiveCasino60DayBonusGBP ,
                LiveCasino60DayActiveDays ,
                MacauTot60DayBet AS MacauTot60DayBetGBP ,
                MacauCount60DayBet ,
                Macau60DayWin AS Macau60DayWinGBP ,
                Macau60DayBonus AS Macau60DayBonusGBP ,
                Macau60DayActiveDays ,
                PokerTot60DayRake AS PokerTot60DayRakeGBP ,
                PokerCount60DayBet ,
                Poker60DayWin AS Poker60DayWinGBP ,
                Poker60DayBonus AS Poker60DayBonusGBP ,
                Poker60DayActiveDays ,
                PokerSideGamesTot60DayBet AS PokerSideGamesTot60DayBetGBP ,
                PokerSideGamesCount60DayBet ,
                PokerSideGames60DayWin AS PokerSideGames60DayWinGBP ,
                PokerSideGames60DayBonus AS PokerSideGames60DayBonusGBP ,
                PokerSideGames60DayActiveDays ,
                ScratchCardsTot60DayBet AS ScratchCardsTot60DayBetGBP ,
                ScratchCardsCount60DayBet ,
                ScratchCards60DayWin AS ScratchCards60DayWinGBP ,
                ScratchCards60DayBonus AS ScratchCards60DayBonusGBP ,
                ScratchCards60DayActiveDays ,
                SkillTot60DayBet AS SkillTot60DayBetGBP ,
                SkillCount60DayBet ,
                Skill60DayWin AS Skill60DayWinGBP ,
                Skill60DayBonus AS Skill60DayBonusGBP ,
                Skill60DayActiveDays ,
                SportsTot60DayBet AS SportsTot60DayBetGBP ,
                SportsCount60DayBet ,
                Sports60DayWin AS Sports60DayWinGBP ,
                Sports60DayBonus AS Sports60DayBonusGBP ,
                Sports60DayActiveDays ,
                VegasTot60DayBet AS VegasTot60DayBetGBP ,
                VegasCount60DayBet ,
                Vegas60DayWin AS Vegas60DayWinGBP ,
                Vegas60DayBonus AS Vegas60DayBonusGBP ,
                Vegas60DayActiveDays ,
                BingoTot180DayBet AS BingoTot180DayBetGBP ,
                BingoCount180DayBet ,
                Bingo180DayWin AS Bingo180DayWinGBP ,
                Bingo180DayBonus AS Bingo180DayBonusGBP ,
                Bingo180DayActiveDays ,
                BingoSideGamesTot180DayBet AS BingoSideGamesTot180DayBetGBP ,
                BingoSideGamesCount180DayBet ,
                BingoSideGames180DayWin AS BingoSideGames180DayWinGBP ,
                BingoSideGames180DayBonus AS BingoSideGames180DayBonusGBP ,
                BingoSideGames180DayActiveDays ,
                CasinoTot180DayBet AS CasinoTot180DayBetGBP ,
                CasinoCount180DayBet ,
                Casino180DayWin AS Casino180DayWinGBP ,
                Casino180DayBonus AS Casino180DayBonusGBP ,
                Casino180DayActiveDays ,
                GamesTot180DayBet AS GamesTot180DayBetGBP ,
                GamesCount180DayBet ,
                Games180DayWin AS Games180DayWinGBP ,
                Games180DayBonus AS Games180DayBonusGBP ,
                Games180DayActiveDays ,
                LiveCasinoTot180DayBet AS LiveCasinoTot180DayBetGBP ,
                LiveCasinoCount180DayBet ,
                LiveCasino180DayWin AS LiveCasino180DayWinGBP ,
                LiveCasino180DayBonus AS LiveCasino180DayBonusGBP ,
                LiveCasino180DayActiveDays ,
                MacauTot180DayBet AS MacauTot180DayBetGBP ,
                MacauCount180DayBet ,
                Macau180DayWin AS Macau180DayWinGBP ,
                Macau180DayBonus AS Macau180DayBonusGBP ,
                Macau180DayActiveDays ,
                PokerTot180DayRake AS PokerTot180DayRakeGBP ,
                PokerCount180DayBet ,
                Poker180DayWin AS Poker180DayWinGBP ,
                Poker180DayBonus AS Poker180DayBonusGBP ,
                Poker180DayActiveDays ,
                PokerSideGamesTot180DayBet AS PokerSideGamesTot180DayBetGBP ,
                PokerSideGamesCount180DayBet ,
                PokerSideGames180DayWin AS PokerSideGames180DayWinGBP ,
                PokerSideGames180DayBonus AS PokerSideGames180DayBonusGBP ,
                PokerSideGames180DayActiveDays ,
                ScratchCardsTot180DayBet AS ScratchCardsTot180DayBetGBP ,
                ScratchCardsCount180DayBet ,
                ScratchCards180DayWin AS ScratchCards180DayWinGBP ,
                ScratchCards180DayBonus AS ScratchCards180DayBonusGBP ,
                ScratchCards180DayActiveDays ,
                SkillTot180DayBet AS SkillTot180DayBetGBP ,
                SkillCount180DayBet ,
                Skill180DayWin AS Skill180DayWinGBP ,
                Skill180DayBonus AS Skill180DayBonusGBP ,
                Skill180DayActiveDays ,
                SportsTot180DayBet AS SportsTot180DayBetGBP ,
                SportsCount180DayBet ,
                Sports180DayWin AS Sports180DayWinGBP ,
                Sports180DayBonus AS Sports180DayBonusGBP ,
                Sports180DayActiveDays ,
                VegasTot180DayBet AS VegasTot180DayBetGBP ,
                VegasCount180DayBet ,
                Vegas180DayWin AS Vegas180DayWinGBP ,
                Vegas180DayBonus AS Vegas180DayBonusGBP ,
                Vegas180DayActiveDays
				-- START 4135 - Added Below column
				,[REDD].[EligibilityFlag]
				,[REDD].[EligibilityDate]
				,[REDD].[OptionInFlag]
				,[REDD].[OptInDate]
				,[REDD].[MaximumStakefactor]
				,[REDD].[InPlayFlag]
				,[REDD].[AccaFlag]
				,[REDD].[MobileFlag]
				-- END 4135
-- TFS 7187
,TotalLifetimeActiveDaysOmni
,TotalLifetimeBetsGBPOmni
,TotalLifetimeDepositsGBPOmni
,TotalLifetimeWithdrawalsGBPOmni
,PriorityFirstChannelId
,PrioritySecondChannelId
,PriorityThirdChannelId
,GraduationDt
,SportsFirstOmniActivityDt
,VegasFirstOmniActivityDt
,CasinoFirstOmniActivityDt
,LiveCasinoFirstOmniActivityDt
,GamesFirstOmniActivityDt
,last_ssbt_bet_date
,CustomerAccountOmniStatusId
,CustomerAccountOmniLinkDt
,next_card_expiry_dt
,reactivation_status_sportsbook
,reactivation_status_vegas
,reactivation_status_macau
,reactivation_status_casino
,reactivation_status_live_casino
,reactivation_status_games
,reactivation_status_scratchcards
,reactivation_status_bingo
,reactivation_status_poker
,customer_second_lifestage_id
,churn_flag
,customer_value_model_unified
,customer_value_model_sportsbook
,customer_value_model_gaming
,LoyaltyPreRestrictedFlag
,LoyaltyPreRestrictedDate
,LoyaltyRestrictedFlag
,LoyaltyRestrictedDate
,LoyaltyBlockedFlag
,LoyaltyBlockedDate
--------------------------------------------------------------------------------------------------------------
-- DANTF-810:
,REDD.CVMGamingRollingNew_PredictedChurnPreventionScore
,REDD.CVMGamingRollingNew_PredictedGWPercGivenActive
,REDD.CVMGamingRollingNew_PredictedNGRGivenActive
,REDD.CVMGamingRollingNew_PredictedProbabilityActive
,REDD.CVMGamingRollingNew_PredictedProbabilityCustomerWins
,REDD.CVMGamingRollingNew_PredictedStakeGivenActive
,REDD.CVMSportsbookRollingNew_PredictedChurnPreventionScore
,REDD.CVMSportsbookRollingNew_PredictedGWPercGivenActive
,REDD.CVMSportsbookRollingNew_PredictedNGRGivenActive
,REDD.CVMSportsbookRollingNew_PredictedProbabilityActive
,REDD.CVMSportsbookRollingNew_PredictedProbabilityCustomerWins
,REDD.CVMSportsbookRollingNew_PredictedStakeGivenActive
,REDD.GamingChurnPrevention_PredictedChurnPreventionScore
,REDD.GamingChurnPrevention_PredictedGWPercGivenActive
,REDD.GamingChurnPrevention_PredictedNGRGivenActive
,REDD.GamingChurnPrevention_PredictedProbabilityActive
,REDD.GamingChurnPrevention_PredictedProbabilityCustomerWins
,REDD.GamingChurnPrevention_PredictedStakeGivenActive
--------------------------------------------------------------------------------------------------------------


        FROM    [dbo].[recipient_extensions_dynamic_data_EXTND] REDD ( NOLOCK )
                LEFT JOIN AdobeAdditionalDays AAD ( NOLOCK ) ON REDD.global_account_id = AAD.CustomerAccountId;


GO

