USE [BusinessSemanticLayer]
GO

DELETE FROM CustomerAccountProfileMeasure WHERE CustomerAccountProfileMeasureNm IN (
'PredictedProbabilityActive'        ,
'PredictedProbabilityCustomerWins'  ,
'PredictedNGRGivenActive'           ,
'PredictedGWPercGivenActive'        ,
'PredictedStakeGivenActive'         ,
'PredictedChurnPreventionScore'     
)


INSERT INTO dbo.CustomerAccountProfileMeasure
(
    CustomerAccountProfileMeasureId,
    CustomerAccountProfileMeasureCd,
    CustomerAccountProfileMeasureNm,
    CustomerAccountProfileMeasureDsc,
    InformationSourceId,
    SystemCreatedDt,
    UpdateByStaffId,
    SystemUpdatedDt
)
VALUES
(   103598, 'PredictedProbabilityActive'        ,'PredictedProbabilityActive', '', 1, GETDATE(), 1, GETDATE()),
(   103599, 'PredictedProbabilityCustomerWins'  ,'PredictedProbabilityCustomerWins', '', 1, GETDATE(), 1, GETDATE()),
(   103600, 'PredictedNGRGivenActive'           ,'PredictedNGRGivenActive', '', 1, GETDATE(), 1, GETDATE()),
(   103601, 'PredictedGWPercGivenActive'        ,'PredictedGWPercGivenActive', '', 1, GETDATE(), 1, GETDATE()),
(   103602, 'PredictedStakeGivenActive'         ,'PredictedStakeGivenActive', '', 1, GETDATE(), 1, GETDATE()),
(   103603, 'PredictedChurnPreventionScore'     ,'PredictedChurnPreventionScore', '', 1, GETDATE(), 1, GETDATE())

/*
95017	PredictedGWPercGivenActive
95019	PredictedProbabilityCustomerWins
95020	PredictedStakeGivenActive
95018	PredictedProbabilityActive
*/
-- to verify:

SELECT * FROM CustomerAccountProfileMeasure WHERE CustomerAccountProfileMeasureNm IN (
'PredictedProbabilityActive'        ,
'PredictedProbabilityCustomerWins'  ,
'PredictedNGRGivenActive'           ,
'PredictedGWPercGivenActive'        ,
'PredictedStakeGivenActive'         ,
'PredictedChurnPreventionScore'     
)

SELECT DISTINCT capm.CustomerAccountProfileMeasureId,
                rmf.CustomerAccountProfileMeasureCd
FROM            MRG.RModelDataFlags rmf
INNER JOIN      dbo.CustomerAccountProfileMeasure AS capm ON capm.CustomerAccountProfileMeasureCd = rmf.CustomerAccountProfileMeasureCd
ORDER BY        rmf.CustomerAccountProfileMeasureCd
/*
103603	[PredictedChurnPreventionScore
103601	[PredictedGWPercGivenActive
103600	[PredictedNGRGivenActive
103598	[PredictedProbabilityActive
103599	[PredictedProbabilityCustomerWins
103602	[PredictedStakeGivenActive
*/
