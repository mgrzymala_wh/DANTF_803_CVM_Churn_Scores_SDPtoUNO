SET NOCOUNT ON

USE [BusinessSemanticLayer]
GO

--DELETE FROM CustomerAccountProfileVal WHERE UpdateByStaffId = 3

DECLARE 
        @MinCustomerAccountProfileTypeId INT
      , @MaxCustomerAccountProfileTypeId INT
      , @MinCustomerAccountProfileMeasureId INT
      , @MaxCustomerAccountProfileMeasureId INT
      , @RandCustomerAccountProfileTypeId INT
      , @RandCustomerAccountProfileMeasureId INT
      , @CustomerAccountId INT
      , @ProfileValNumber DECIMAL (20, 2)
      , @ProfileValtext VARCHAR (50)
      , @SystemCreatedDt DATETIME = N'2020-12-31T07:33:23.737'
      , @SystemUpdatedDt DATETIME = N'2020-12-31T07:33:23.737'
      
      , @Counter INT = 0
      , @MaxInsertCountPerCustomer INT = 5

SELECT 
        @MinCustomerAccountProfileTypeId = MIN(CustomerAccountProfileTypeId) ,
        @MaxCustomerAccountProfileTypeId = MAX(CustomerAccountProfileTypeId) 

FROM    [BusinessSemanticLayer].[dbo].[CustomerAccountProfileType] 
WHERE   CustomerAccountProfileTypeNm IN (
       'CVMGamingRollingNew'
      ,'CVMSportsbookRollingNew'
      ,'GamingChurnPrevention'
)

SELECT 
        @MinCustomerAccountProfileMeasureId = MIN(CustomerAccountProfileMeasureId) ,
        @MaxCustomerAccountProfileMeasureId = MAX(CustomerAccountProfileMeasureId) 

FROM    [BusinessSemanticLayer].[dbo].[CustomerAccountProfileMeasure] 
WHERE   CustomerAccountProfileMeasureNm IN (
       'PredictedProbabilityActive'        ,
       'PredictedProbabilityCustomerWins'  ,
       'PredictedNGRGivenActive'           ,
       'PredictedGWPercGivenActive'        ,
       'PredictedStakeGivenActive'         ,
       'PredictedChurnPreventionScore'     
)

DECLARE my_cursor CURSOR FOR 
SELECT  t.global_account_id FROM (
                                   SELECT TOP 2500 global_account_id FROM [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data] ORDER BY NEWID()
                                 ) t ORDER BY 1
	
OPEN my_cursor   
FETCH NEXT FROM my_cursor INTO @CustomerAccountId  
WHILE @@FETCH_STATUS = 0   
BEGIN
	SET @Counter = 0
    WHILE (@Counter < @MaxInsertCountPerCustomer)
    BEGIN

    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        SELECT @RandCustomerAccountProfileTypeId = @MinCustomerAccountProfileTypeId + CONVERT(INT, (@MaxCustomerAccountProfileTypeId - @MinCustomerAccountProfileTypeId + 1) * RAND())
        --SELECT @RandCustomerAccountProfileTypeId 
        SELECT @RandCustomerAccountProfileMeasureId = @MinCustomerAccountProfileMeasureId + CONVERT(INT, (@MaxCustomerAccountProfileMeasureId - @MinCustomerAccountProfileMeasureId + 1) * RAND())
        --SELECT @RandCustomerAccountProfileMeasureId
        
        SELECT @ProfileValNumber = CAST(RAND(CHECKSUM(NEWID()))*1000 AS DECIMAL(20, 2))
        SELECT @ProfileValtext = CAST(@ProfileValNumber AS VARCHAR (50))
    
        INSERT INTO [BusinessSemanticLayer].[dbo].[CustomerAccountProfileVal]
        (
            CustomerAccountProfileTypeId,
            CustomerAccountProfileMeasureId,
            CustomerAccountId,
            CustomerAccountValNumber,
            CustomerAccountValTxt,
            InformationSourceId,
            SystemCreatedDt,
            UpdateByStaffId,
            SystemUpdatedDt
        )
        VALUES
        -- number of lines below must match @MaxInsertCountPerCustomer:
        ( @MinCustomerAccountProfileTypeId + CONVERT(INT, (@MaxCustomerAccountProfileTypeId - @MinCustomerAccountProfileTypeId + 1) * RAND()), @MinCustomerAccountProfileMeasureId + CONVERT(INT, (@MaxCustomerAccountProfileMeasureId - @MinCustomerAccountProfileMeasureId + 1) * RAND()), @CustomerAccountId, @ProfileValNumber*101,  CAST(@ProfileValNumber*101 AS VARCHAR (50)), 1, @SystemCreatedDt, 3, @SystemUpdatedDt ),
        ( @MinCustomerAccountProfileTypeId + CONVERT(INT, (@MaxCustomerAccountProfileTypeId - @MinCustomerAccountProfileTypeId + 1) * RAND()), @MinCustomerAccountProfileMeasureId + CONVERT(INT, (@MaxCustomerAccountProfileMeasureId - @MinCustomerAccountProfileMeasureId + 1) * RAND()), @CustomerAccountId, @ProfileValNumber*103,  CAST(@ProfileValNumber*103 AS VARCHAR (50)), 1, @SystemCreatedDt, 3, @SystemUpdatedDt ),
        ( @MinCustomerAccountProfileTypeId + CONVERT(INT, (@MaxCustomerAccountProfileTypeId - @MinCustomerAccountProfileTypeId + 1) * RAND()), @MinCustomerAccountProfileMeasureId + CONVERT(INT, (@MaxCustomerAccountProfileMeasureId - @MinCustomerAccountProfileMeasureId + 1) * RAND()), @CustomerAccountId, @ProfileValNumber*107,  CAST(@ProfileValNumber*107 AS VARCHAR (50)), 1, @SystemCreatedDt, 3, @SystemUpdatedDt ),
        ( @MinCustomerAccountProfileTypeId + CONVERT(INT, (@MaxCustomerAccountProfileTypeId - @MinCustomerAccountProfileTypeId + 1) * RAND()), @MinCustomerAccountProfileMeasureId + CONVERT(INT, (@MaxCustomerAccountProfileMeasureId - @MinCustomerAccountProfileMeasureId + 1) * RAND()), @CustomerAccountId, @ProfileValNumber*109,  CAST(@ProfileValNumber*109 AS VARCHAR (50)), 1, @SystemCreatedDt, 3, @SystemUpdatedDt ),
        ( @MinCustomerAccountProfileTypeId + CONVERT(INT, (@MaxCustomerAccountProfileTypeId - @MinCustomerAccountProfileTypeId + 1) * RAND()), @MinCustomerAccountProfileMeasureId + CONVERT(INT, (@MaxCustomerAccountProfileMeasureId - @MinCustomerAccountProfileMeasureId + 1) * RAND()), @CustomerAccountId, @ProfileValNumber*113,  CAST(@ProfileValNumber*113 AS VARCHAR (50)), 1, @SystemCreatedDt, 3, @SystemUpdatedDt )
    
        IF @@ROWCOUNT = @MaxInsertCountPerCustomer 
        BEGIN
            PRINT CONCAT(
                          'Finished inserting record with CustomerAccountId: ', CAST(@CustomerAccountId AS NVARCHAR(32))
                       , ' RandCustomerAccountProfileTypeId: ', CAST(@RandCustomerAccountProfileTypeId AS NVARCHAR(32))
                       , ' RandCustomerAccountProfileMeasureId: ', CAST(@RandCustomerAccountProfileMeasureId AS NVARCHAR(32))
                       , ' ProfileValtext: ', @ProfileValtext
                       )
        END
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	
    SET @Counter = @Counter + 1
    END

    FETCH NEXT FROM my_cursor INTO @CustomerAccountId  
END   

CLOSE my_cursor   
DEALLOCATE my_cursor


