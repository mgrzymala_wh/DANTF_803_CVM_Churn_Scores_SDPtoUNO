USE [BusinessSemanticLayer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================================================  
-- Author:  WHGROUP\mgrzymala    
-- Create date: 2020-12-09   
-- Description: Create  [src].[usp_Populate_RModelDataFlags_Pivoted_All]  
--       
-- =======================================================================================================================================

-- to run:
-- TRUNCATE TABLE [BusinessSemanticLayer].[MRG].[RModelDataFlags_Pivoted_All]
/*
DECLARE 
    @_StartDate DATETIME = GETDATE() -1,
    @_EndDate DATETIME   = GETDATE() + 30

EXEC [BusinessSemanticLayer].[MRG].[usp_Populate_RModelDataFlags_Pivoted_All] @StartDate = @_StartDate, @EndDate = @_EndDate
-- to test:
SELECT   * --COUNT(*) 
FROM     [BusinessSemanticLayer].[MRG].[RModelDataFlags_Pivoted_All]
WHERE    CustomerAccountId = 14427257

-- compare with:
SELECT 
            CAPV.CustomerAccountId,
            CAPT.CustomerAccountProfileTypeCd,
            CAPM.CustomerAccountProfileMeasureCd,
            CAPV.CustomerAccountValTxt
FROM        [BusinessSemanticLayer].[dbo].CustomerAccountProfileVal CAPV
INNER JOIN  [BusinessSemanticLayer].[dbo].CustomerAccountProfileType AS CAPT ON CAPT.CustomerAccountProfileTypeId = CAPV.CustomerAccountProfileTypeId
INNER JOIN  [BusinessSemanticLayer].[dbo].CustomerAccountProfileMeasure AS CAPM ON CAPM.CustomerAccountProfileMeasureId = CAPV.CustomerAccountProfileMeasureId
WHERE       CustomerAccountId = 4203456

ORDER BY CustomerAccountId, CustomerAccountProfileTypeCd, CustomerAccountProfileMeasureCd
*/
-- =======================================================================================================================================  
-- Change History   
-- Date         User						Change                   
-- 2020-12-09   mgrzymala                   DANTF-810 - initial version
-- =======================================================================================================================================  

CREATE OR ALTER PROCEDURE [MRG].[usp_Populate_RModelDataFlags_Pivoted_All]
@StartDate DATETIME, @EndDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET NOCOUNT ON;
SET XACT_ABORT ON;

DECLARE 
    
     @ExecStartTime      DATETIME2(7)
    ,@ExecFinishTime     DATETIME2(7)
    ,@ExecDurationSec    INT
    ,@ErrMsg		     VARCHAR(MAX) 
	,@CustomErrorMessage VARCHAR(2048)
    ,@ErrorSeverity	     INT 
    ,@ErrorNumber	     INT
	,@ErrorState	     INT 
	,@ErrorLine		     INT 
	,@ErrorMessage	     VARCHAR(1000) 
    ,@@MESSAGE           VARCHAR(2048)
    ,@@PROCNAME          VARCHAR(255) 
    ,@@USERNAME          VARCHAR(32)

BEGIN TRY
SELECT @ExecStartTime = GETDATE()

DROP TABLE IF EXISTS #tempCustLists;
------------------------------------------------------------------------------------------------------------------------------------
     CREATE table #DomainCategory
            (
              ChannelId INT 
            );

        INSERT  INTO #DomainCategory
                ( ChannelId 
                )
                SELECT DISTINCT
                        ChannelId 
                FROM    [dbo].[Channel] C
                        INNER JOIN dbo.Domain D ON C.DomainId = D.DomainId
                        INNER JOIN [dbo].[DomainCategory] DC ON D.[DomainCategoryId] = DC.[DomainCategoryId]
                WHERE   DC.[DomainCategoryCd] NOT IN ( 'US' );

WITH custs
  AS (
      SELECT ca.CustomerAccountId
        FROM dbo.CustomerAccount AS ca (NOLOCK)
       WHERE ca.SystemUpdatedDt > @StartDate
         AND ca.SystemUpdatedDt <= @EndDate
      UNION
      SELECT cap.CustomerAccountId
        FROM dbo.CustomerAccountProfile AS cap (NOLOCK)
       WHERE cap.SystemUpdatedDt > @StartDate
         AND cap.SystemUpdatedDt <= @EndDate
      UNION
      SELECT cafp.CustomerAccountId
        FROM dbo.CustomerAccountFlagProfile AS cafp (NOLOCK)
       WHERE cafp.SystemUpdatedDt > @StartDate
         AND cafp.SystemUpdatedDt <= @EndDate
      UNION
      SELECT DISTINCT q1.CustomerAccountId
        FROM MRG.CustomerSourceBettingDailyAggregateRolling (NOLOCK) q1
       WHERE q1.SystemCreatedDt > @StartDate
         AND q1.SystemCreatedDt <=@EndDate
      UNION
      SELECT DISTINCT q1.CustomerAccountId
        FROM MRG.CustomerSourceDepositDailyAggregateRolling (NOLOCK) q1
       WHERE q1.SystemCreatedDt > @StartDate
         AND q1.SystemCreatedDt <= @EndDate
	  UNION
      SELECT DISTINCT capv.CustomerAccountId
        FROM dbo.CustomerAccountProfileVal AS capv (NOLOCK)
       WHERE capv.SystemUpdatedDt > @StartDate
         AND capv.SystemUpdatedDt <= @EndDate
	  UNION
      SELECT DISTINCT cadp.CustomerAccountId
        FROM dbo.CustomerAccountDerivedProfile AS cadp (NOLOCK)
       WHERE cadp.SystemUpdatedDt > @StartDate
         AND cadp.SystemUpdatedDt <= @EndDate
	  UNION
      SELECT DISTINCT CAPR.CustomerAccountId
        FROM dbo.CustomerAccountProductReactivation AS CAPR (NOLOCK)
       WHERE CAPR.SystemUpdatedDt > @StartDate
         AND CAPR.SystemUpdatedDt <= @EndDate
	  UNION
      SELECT DISTINCT pc.CustomerAccountId
        FROM dbo.PaymentCard AS pc (NOLOCK)
       --WHERE pc.SystemUpdatedDt > @StartDate
       --  AND pc.SystemUpdatedDt <= @EndDate		 
		 
		 )
SELECT          custs.CustomerAccountId
INTO            #tempCustLists
FROM            custs
INNER JOIN      dbo.vwCustomerAccount (NOLOCK) AS vca
INNER JOIN      #DomainCategory AS dc ON dc.ChannelId = vca.RegistrationChannelId  -- TFS 20541 - Adding filter of RTBF and US data to not send to Adobe
INNER JOIN      dbo.CustomerAccountFlagProfile AS cafp ON cafp.CustomerAccountId = vca.CustomerAccountId  -- TFS 20541 - Adding filter of RTBF and US data to not send to Adobe
ON              vca.CustomerAccountId = custs.CustomerAccountId
AND             vca.IntegratedAccountFlag='Y'
AND             cafp.RightToBeForgottenFlag = 'N'	;  --  TFS 20541 - Adding filter of RTBF and US data to not send to Adobe

--SELECT COUNT(*) FROM #tempCustLists

------------------------------------------------START TFS 16162---------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
--Create a dataset from CustomerAccountProfileVal that contains only the latest value of each combination of Type and Measure for each customerid
TRUNCATE TABLE  MRG.RModelDataFlags
INSERT INTO     MRG.RModelDataFlags
        ( CustomerAccountId ,
          CustomerAccountValTxt ,
		  CustomerAccountProfileTypeCd,
          CustomerAccountProfileMeasureCd

        )
        SELECT  RMC.CustomerAccountId ,
                RMC.CustomerAccountValTxt ,
				RMC.CustomerAccountProfileTypeCd,
                RMC.CustomerAccountProfileMeasureCd

        FROM    ( SELECT    CAPV.CustomerAccountId ,
                            CAPV.CustomerAccountValTxt,
							CAPT.CustomerAccountProfileTypeCd,
                            CAPM.CustomerAccountProfileMeasureCd,

                            RN = ROW_NUMBER() OVER ( PARTITION BY CAPV.CustomerAccountId, CAPV.CustomerAccountProfileTypeId, CAPV.CustomerAccountProfileMeasureId 
                                                     ORDER BY     CAPV.SystemUpdatedDt DESC
                                                                , CAPV.CustomerAccountValNumber DESC-- <== if we have more than one value with the same Type, Measure and SystemUpdatedDt combination 
                                                                                                         --then let's take the largest one
                                                     ) 
                  FROM      dbo.CustomerAccountProfileVal AS CAPV 
							INNER JOIN #tempCustLists AS TCL ON TCL.CustomerAccountId = CAPV.CustomerAccountId
                            INNER JOIN dbo.CustomerAccountProfileType AS CAPT ON CAPT.CustomerAccountProfileTypeId = CAPV.CustomerAccountProfileTypeId
                            INNER JOIN dbo.CustomerAccountProfileMeasure AS CAPM ON CAPM.CustomerAccountProfileMeasureId = CAPV.CustomerAccountProfileMeasureId
                  WHERE     CAPV.SystemUpdatedDt >= @StartDate
				  AND       CAPV.SystemUpdatedDt < @EndDate
                ) RMC
        WHERE   RMC.RN = 1;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CustomerAccountProfileTypeNm:  CVMGamingRollingNew:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--SELECT COUNT(*) FROM  dbo.CustomerAccountProfileVal WHERE CustomerAccountProfileTypeId IS NOT NULL AND CustomerAccountProfileMeasureId IS NOT NULL

DECLARE @_CustomerAccountProfileMeasureCds AS [MRG].[CustomerAccountProfileMeasureCds]
INSERT INTO @_CustomerAccountProfileMeasureCds VALUES 
 ('PredictedChurnPreventionScore')
,('PredictedGWPercGivenActive')
,('PredictedNGRGivenActive')
,('PredictedProbabilityActive')
,('PredictedProbabilityCustomerWins')
,('PredictedStakeGivenActive')

TRUNCATE TABLE  [MRG].[RModelDataFlags_Pivoted_CVMGamingRollingNew]
INSERT INTO     [MRG].[RModelDataFlags_Pivoted_CVMGamingRollingNew] (
                [CustomerAccountId]
               ,[CVMGamingRollingNew_PredictedChurnPreventionScore]
               ,[CVMGamingRollingNew_PredictedGWPercGivenActive]
               ,[CVMGamingRollingNew_PredictedNGRGivenActive]
               ,[CVMGamingRollingNew_PredictedProbabilityActive]
               ,[CVMGamingRollingNew_PredictedProbabilityCustomerWins]
               ,[CVMGamingRollingNew_PredictedStakeGivenActive]
)
EXEC            [MRG].[usp_Populate_RModelDataFlags_Pivoted_Tables] @CustomerAccountProfileTypeCd = 'CVMGamingRollingNew', @CustomerAccountProfileMeasureCds = @_CustomerAccountProfileMeasureCds
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CustomerAccountProfileTypeNm:  CVMSportsbookRollingNew:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE  [MRG].[RModelDataFlags_Pivoted_CVMSportsbookRollingNew]
INSERT INTO     [MRG].[RModelDataFlags_Pivoted_CVMSportsbookRollingNew] (
                [CustomerAccountId]
               ,[CVMSportsbookRollingNew_PredictedChurnPreventionScore]
               ,[CVMSportsbookRollingNew_PredictedGWPercGivenActive]
               ,[CVMSportsbookRollingNew_PredictedNGRGivenActive]
               ,[CVMSportsbookRollingNew_PredictedProbabilityActive]
               ,[CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]
               ,[CVMSportsbookRollingNew_PredictedStakeGivenActive]
)
EXEC            [MRG].[usp_Populate_RModelDataFlags_Pivoted_Tables] @CustomerAccountProfileTypeCd = 'CVMSportsbookRollingNew', @CustomerAccountProfileMeasureCds = @_CustomerAccountProfileMeasureCds
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CustomerAccountProfileTypeNm:  GamingChurnPrevention:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE  [MRG].[RModelDataFlags_Pivoted_GamingChurnPrevention]
INSERT INTO     [MRG].[RModelDataFlags_Pivoted_GamingChurnPrevention] (
                [CustomerAccountId]
               ,[GamingChurnPrevention_PredictedChurnPreventionScore]
               ,[GamingChurnPrevention_PredictedGWPercGivenActive]
               ,[GamingChurnPrevention_PredictedNGRGivenActive]
               ,[GamingChurnPrevention_PredictedProbabilityActive]
               ,[GamingChurnPrevention_PredictedProbabilityCustomerWins]
               ,[GamingChurnPrevention_PredictedStakeGivenActive]
)
EXEC            [MRG].[usp_Populate_RModelDataFlags_Pivoted_Tables] @CustomerAccountProfileTypeCd = 'GamingChurnPrevention', @CustomerAccountProfileMeasureCds = @_CustomerAccountProfileMeasureCds
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     JOINING PIVOTS FOR ALL 3 TYPES:    $    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE   [MRG].[RModelDataFlags_Pivoted_All]
INSERT INTO      [MRG].[RModelDataFlags_Pivoted_All] (
                 [CustomerAccountId]                                       
                ,[CVMGamingRollingNew_PredictedChurnPreventionScore]       
                ,[CVMGamingRollingNew_PredictedGWPercGivenActive]          
                ,[CVMGamingRollingNew_PredictedNGRGivenActive]             
                ,[CVMGamingRollingNew_PredictedProbabilityActive]          
                ,[CVMGamingRollingNew_PredictedProbabilityCustomerWins]    
                ,[CVMGamingRollingNew_PredictedStakeGivenActive]           
                ,[CVMSportsbookRollingNew_PredictedChurnPreventionScore]   
                ,[CVMSportsbookRollingNew_PredictedGWPercGivenActive]      
                ,[CVMSportsbookRollingNew_PredictedNGRGivenActive]         
                ,[CVMSportsbookRollingNew_PredictedProbabilityActive]      
                ,[CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]
                ,[CVMSportsbookRollingNew_PredictedStakeGivenActive]       
                ,[GamingChurnPrevention_PredictedChurnPreventionScore]     
                ,[GamingChurnPrevention_PredictedGWPercGivenActive]        
                ,[GamingChurnPrevention_PredictedNGRGivenActive]           
                ,[GamingChurnPrevention_PredictedProbabilityActive]        
                ,[GamingChurnPrevention_PredictedProbabilityCustomerWins]  
                ,[GamingChurnPrevention_PredictedStakeGivenActive]         
)

SELECT          DISTINCT 
                rmdf.CustomerAccountId
               ,grn.[CVMGamingRollingNew_PredictedChurnPreventionScore]
               ,grn.[CVMGamingRollingNew_PredictedGWPercGivenActive]
               ,grn.[CVMGamingRollingNew_PredictedNGRGivenActive]
               ,grn.[CVMGamingRollingNew_PredictedProbabilityActive]
               ,grn.[CVMGamingRollingNew_PredictedProbabilityCustomerWins]
               ,grn.[CVMGamingRollingNew_PredictedStakeGivenActive]
               
               ,srn.[CVMSportsbookRollingNew_PredictedChurnPreventionScore]
               ,srn.[CVMSportsbookRollingNew_PredictedGWPercGivenActive]
               ,srn.[CVMSportsbookRollingNew_PredictedNGRGivenActive]
               ,srn.[CVMSportsbookRollingNew_PredictedProbabilityActive]
               ,srn.[CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]
               ,srn.[CVMSportsbookRollingNew_PredictedStakeGivenActive]
               
               ,gcp.[GamingChurnPrevention_PredictedChurnPreventionScore]
               ,gcp.[GamingChurnPrevention_PredictedGWPercGivenActive]
               ,gcp.[GamingChurnPrevention_PredictedNGRGivenActive]
               ,gcp.[GamingChurnPrevention_PredictedProbabilityActive]
               ,gcp.[GamingChurnPrevention_PredictedProbabilityCustomerWins]
               ,gcp.[GamingChurnPrevention_PredictedStakeGivenActive]

FROM            MRG.RModelDataFlags rmdf
LEFT JOIN       MRG.RModelDataFlags_Pivoted_CVMGamingRollingNew      grn ON grn.CustomerAccountId = rmdf.CustomerAccountId 
LEFT JOIN       MRG.RModelDataFlags_Pivoted_CVMSportsbookRollingNew  srn ON srn.CustomerAccountId = rmdf.CustomerAccountId
LEFT JOIN       MRG.RModelDataFlags_Pivoted_GamingChurnPrevention    gcp ON gcp.CustomerAccountId = rmdf.CustomerAccountId

--WHERE rmdf.CustomerAccountId IN (40161111, 40341300, 40261053, 39464481)
ORDER BY rmdf.CustomerAccountId

SELECT @ExecFinishTime  = GETDATE()

SELECT @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')
SELECT @@MESSAGE = 'Procedure ' +@@PROCNAME+ ' executed successfully with duration of: ' +CAST(DATEDIFF(SECOND, @ExecStartTime, @ExecFinishTime) AS NVARCHAR(32))+' seconds'
EXEC   xp_logevent 60000, @@MESSAGE, informational WITH RESULT SETS NONE; 

END TRY
BEGIN CATCH
	SET @ErrMsg = 'Error executing: '   + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown') + CHAR(13) 
	+ 'Error Procedure: '               + COALESCE(ERROR_PROCEDURE(), 'Unknown')
	+ ', Error Number: '                + CAST(ERROR_NUMBER() AS VARCHAR)
	+ ', Error Severity: '              + CAST(ERROR_SEVERITY() AS VARCHAR)
	+ ', Error State: '                 + CAST(ERROR_STATE() AS VARCHAR)
	+ ', Error Line: '                  + CAST(ERROR_LINE() AS VARCHAR)
	+ ', Error Message: '               + COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)
	+ ', Server Name: '                 + @@SERVERNAME + ' - Database: ' + DB_NAME() + ', User: ' + SUSER_NAME() + ', SPID: ' + CAST(@@SPID AS VARCHAR) + ', Now: ' + CAST(GETDATE() AS VARCHAR(121));
   SET @ErrMsg = CONCAT(@ErrMsg, ' ', @CustomErrorMessage)
   RAISERROR (@ErrMsg, @ErrorSeverity, @ErrorState);
END CATCH;

END