USE [StagingTeliumOutbound]
GO

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE [StagingTeliumOutbound].[dbo].[recipientextensionsdynamicdata_EXTND]
SELECT * FROM [StagingTeliumOutbound].[dbo].[recipientextensionsdynamicdata_EXTND]

DECLARE @execution_id bigint
EXEC [SSISDBLINK].[SSISDB].[catalog].[create_execution] @package_name=N'REF_DIP_RecipientExtensionsDynamicData_BSL TL002.dtsx', @execution_id=@execution_id OUTPUT, @folder_name=N'DataManagementPlatform', @project_name=N'BSL_TL_Integration', @use32bitruntime=False, @reference_id=198
Select @execution_id
DECLARE @var0 smallint = 1
EXEC [SSISDBLINK].[SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'LOGGING_LEVEL', @parameter_value=@var0
EXEC [SSISDBLINK].[SSISDB].[catalog].[set_execution_property_override_value] @execution_id,  @property_path=N'\Package.Variables[User::gv_CurrReqId]', @property_value=N'34', @sensitive=False
EXEC [SSISDBLINK].[SSISDB].[catalog].[start_execution] @execution_id
GO

SET NUMERIC_ROUNDABORT OFF

SELECT COUNT(*) FROM [StagingTeliumOutbound].[dbo].[recipientextensionsdynamicdata_EXTND] 
SELECT --* 
CustomerAccountId
, CVMGamingRollingNew_PredictedProbabilityActive
, CVMGamingRollingNew_PredictedNGRGivenActive
, [cvm_predicted_ngr_gaming]
, CVMSportsbookRollingNew_PredictedProbabilityActive
, CVMSportsbookRollingNew_PredictedStakeGivenActive
, [cvm_prodicted_ngr_sportsbook]

FROM [StagingTeliumOutbound].[dbo].[recipientextensionsdynamicdata_EXTND] WHERE [cvm_predicted_ngr_gaming] IS NULL AND [cvm_prodicted_ngr_sportsbook] IS NOT NULL
--SELECT 82181.35 * 10091.22
--829310082.7470
--829310082.7470

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE [StagingTeliumOutbound].[dbo].[ExportDailyCustomers]
SELECT * FROM [StagingTeliumOutbound].[dbo].[ExportDailyCustomers]

Declare @execution_id bigint
EXEC [SSISDBLINK].[SSISDB].[catalog].[create_execution] @package_name=N'REF_DIP_CombineData_BSL TL005.dtsx', @execution_id=@execution_id OUTPUT, @folder_name=N'DataManagementPlatform', @project_name=N'BSL_TL_Integration', @use32bitruntime=False, @reference_id=198
Select @execution_id
DECLARE @var0 smallint = 1
EXEC [SSISDBLINK].[SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'LOGGING_LEVEL', @parameter_value=@var0
EXEC [SSISDBLINK].[SSISDB].[catalog].[set_execution_property_override_value] @execution_id,  @property_path=N'\Package.Variables[User::gv_CurrReqId]', @property_value=N'34', @sensitive=False
EXEC [SSISDBLINK].[SSISDB].[catalog].[start_execution] @execution_id
GO
SELECT * FROM [StagingTeliumOutbound].[dbo].[ExportDailyCustomers] ORDER BY CustomerAccountId 
-- with CROSS APPLY:       448
-- without CROSS APPLY:    144
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Declare @execution_id bigint
EXEC [SSISDBLINK].[SSISDB].[catalog].[create_execution] @package_name=N'REF_DDF_BlacklistFlags_TL_OUT001.dtsx', @execution_id=@execution_id OUTPUT, @folder_name=N'DataManagementPlatform', @project_name=N'TL_OUT_DataDelivery', @use32bitruntime=False, @reference_id=199
Select @execution_id
DECLARE @var0 smallint = 1
EXEC [SSISDBLINK].[SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'LOGGING_LEVEL', @parameter_value=@var0
EXEC [SSISDBLINK].[SSISDB].[catalog].[set_execution_property_override_value] @execution_id,  @property_path=N'\Package.Variables[User::gv_CurrReqId]', @property_value=N'34', @sensitive=False
EXEC [SSISDBLINK].[SSISDB].[catalog].[start_execution] @execution_id
GO
-- Check file customers_YYYYMMDD in path C:\ClusterStorage\Acquisition\DataPlatform\Output\Telium
--------------------------------------------------------
DECLARE @DateSince DATE = GETDATE() - 30

SELECT
    ei.execution_id,
    ei.folder_name,
    ei.project_name,
    ei.package_name,
    CAST(ei.start_time AS DATETIME2(0))             AS [StartTime],
    CAST(ei.end_time AS DATETIME2(0))               AS [EndTime],
    CASE ei.status 
          WHEN 1 THEN 'Created'
          WHEN 2 THEN 'Running'
          WHEN 3 THEN 'Cancelled'
          WHEN 4 THEN 'Failed'
          WHEN 5 THEN 'About to run'
          WHEN 6 THEN 'Ended unexpectedly'
          WHEN 7 THEN 'Success'
          WHEN 8 THEN 'Stopping'
          WHEN 9 THEN 'Completed'
          ELSE 'Unknown'
    END AS [Status],
    DATEDIFF(SECOND, ei.start_time, ei.end_time)    AS [execution_time (sec)],
    DATEDIFF(MINUTE, ei.start_time, ei.end_time)    AS [execution_time (min)],
    ei.environment_folder_name,
    ei.environment_name,
    ei.executed_as_name,
    ov.property_path,
    ov.property_value,
    ei.use32bitruntime,
    ei.operation_type,
    CAST(ei.created_time AS DATETIME2(0))             AS [CreatedTime],
    ei.object_type

FROM           [SSISDBLINK].[SSISDB].internal.execution_info ei WITH (NOLOCK)
LEFT JOIN      [SSISDBLINK].[SSISDB].catalog.execution_property_override_values ov WITH (NOLOCK)  ON ov.execution_id = ei.execution_id

WHERE
        ei.package_name --= @PackageName AND
        
        IN (
              'REF_DIP_RecipientExtensionsDynamicData_BSL TL002.dtsx', 'REF_DIP_CombineData_BSL TL005.dtsx', 'REF_DDF_BlacklistFlags_TL_OUT001.dtsx'
        ) AND 
              
        ei.start_time >= @DateSince

ORDER BY ei.start_time DESC