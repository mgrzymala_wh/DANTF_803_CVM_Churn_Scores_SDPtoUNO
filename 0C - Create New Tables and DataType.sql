USE [BusinessSemanticLayer]
GO

DROP TABLE IF EXISTS MRG.RModelDataFlags;
CREATE TABLE MRG.RModelDataFlags
(
  CustomerAccountId INT ,
  CustomerAccountValTxt VARCHAR(50) ,
  CustomerAccountProfileTypeCd VARCHAR(50),      --CustomerAccountProfileTypeId INT
  CustomerAccountProfileMeasureCd VARCHAR(50) , --CustomerAccountProfileMeasureId INT
)

CREATE NONCLUSTERED INDEX [IX_NCL_RModelDataFlags_CustomerAccountId]
ON [MRG].[RModelDataFlags] ([CustomerAccountId])
INCLUDE ([CustomerAccountValTxt],[CustomerAccountProfileTypeCd], [CustomerAccountProfileMeasureCd])
GO

DROP TABLE IF EXISTS [MRG].[RModelDataFlags_Pivoted_CVMGamingRollingNew];
CREATE TABLE [MRG].[RModelDataFlags_Pivoted_CVMGamingRollingNew]
(
    [CustomerAccountId]                                         INT NOT NULL                                               
   ,[CVMGamingRollingNew_PredictedChurnPreventionScore]         VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMGamingRollingNew_PredictedGWPercGivenActive]            VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMGamingRollingNew_PredictedNGRGivenActive]               VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMGamingRollingNew_PredictedProbabilityActive]            VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMGamingRollingNew_PredictedProbabilityCustomerWins]      VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMGamingRollingNew_PredictedStakeGivenActive]             VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
CREATE CLUSTERED INDEX [IX_CL_RModelDataFlags_Pivoted_CVMGamingRollingNew]
ON [MRG].[RModelDataFlags_Pivoted_CVMGamingRollingNew] ([CustomerAccountId])
GO

DROP TABLE IF EXISTS [MRG].[RModelDataFlags_Pivoted_CVMSportsbookRollingNew];
CREATE TABLE [MRG].[RModelDataFlags_Pivoted_CVMSportsbookRollingNew]
(
    [CustomerAccountId]                                             INT NOT NULL                                               
   ,[CVMSportsbookRollingNew_PredictedChurnPreventionScore]         VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMSportsbookRollingNew_PredictedGWPercGivenActive]            VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMSportsbookRollingNew_PredictedNGRGivenActive]               VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMSportsbookRollingNew_PredictedProbabilityActive]            VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]      VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[CVMSportsbookRollingNew_PredictedStakeGivenActive]             VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
CREATE CLUSTERED INDEX [IX_CL_RModelDataFlags_Pivoted_CVMSportsbookRollingNew]
ON [MRG].[RModelDataFlags_Pivoted_CVMSportsbookRollingNew] ([CustomerAccountId])
GO

DROP TABLE IF EXISTS [MRG].[RModelDataFlags_Pivoted_GamingChurnPrevention];
CREATE TABLE [MRG].[RModelDataFlags_Pivoted_GamingChurnPrevention]
(
    [CustomerAccountId]                                             INT NOT NULL                                               
   ,[GamingChurnPrevention_PredictedChurnPreventionScore]         VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[GamingChurnPrevention_PredictedGWPercGivenActive]            VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[GamingChurnPrevention_PredictedNGRGivenActive]               VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[GamingChurnPrevention_PredictedProbabilityActive]            VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[GamingChurnPrevention_PredictedProbabilityCustomerWins]      VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
   ,[GamingChurnPrevention_PredictedStakeGivenActive]             VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
CREATE CLUSTERED INDEX [IX_CL_RModelDataFlags_Pivoted_GamingChurnPrevention]
ON [MRG].[RModelDataFlags_Pivoted_GamingChurnPrevention] ([CustomerAccountId])
GO

DROP TABLE IF EXISTS [MRG].[RModelDataFlags_Pivoted_All]
CREATE TABLE [MRG].[RModelDataFlags_Pivoted_All]
(
    [CustomerAccountId]                                         INT NULL,
    [CVMGamingRollingNew_PredictedChurnPreventionScore]         VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMGamingRollingNew_PredictedGWPercGivenActive]            VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMGamingRollingNew_PredictedNGRGivenActive]               VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMGamingRollingNew_PredictedProbabilityActive]            VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMGamingRollingNew_PredictedProbabilityCustomerWins]      VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMGamingRollingNew_PredictedStakeGivenActive]             VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMSportsbookRollingNew_PredictedChurnPreventionScore]     VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMSportsbookRollingNew_PredictedGWPercGivenActive]        VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMSportsbookRollingNew_PredictedNGRGivenActive]           VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMSportsbookRollingNew_PredictedProbabilityActive]        VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]  VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CVMSportsbookRollingNew_PredictedStakeGivenActive]         VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [GamingChurnPrevention_PredictedChurnPreventionScore]       VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [GamingChurnPrevention_PredictedGWPercGivenActive]          VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [GamingChurnPrevention_PredictedNGRGivenActive]             VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [GamingChurnPrevention_PredictedProbabilityActive]          VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [GamingChurnPrevention_PredictedProbabilityCustomerWins]    VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [GamingChurnPrevention_PredictedStakeGivenActive]           VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
CREATE CLUSTERED INDEX [IX_CL_RModelDataFlags_Pivoted_All]
ON [MRG].[RModelDataFlags_Pivoted_All] ([CustomerAccountId])
GO


--DROP PROCEDURE mrg.usp_Populate_RModelDataFlags_Pivoted_Tables

DROP TYPE IF EXISTS [MRG].[CustomerAccountProfileMeasureCds];
CREATE TYPE [MRG].[CustomerAccountProfileMeasureCds] AS TABLE(
	[CustomerAccountProfileMeasureCd] VARCHAR(50) NOT NULL
);
GO

USE [StagingAdobeOutbound]
GO

DROP TABLE IF EXISTS [dbo].[recipient_extensions_dynamic_data_EXTND]
CREATE TABLE [dbo].[recipient_extensions_dynamic_data_EXTND]
(
    [global_account_id] INT,
    [risk_profile_id] INT,
    [viplevel_casino_id] INT,
    [viplevel_poker_id] INT,
    [viplevel_vegas_id] INT,
    [account_balance] MONEY,
    [last_login_date] DATETIME,
    [last_system_id] INT,
    [last_withdrawal_date] DATETIME,
    [last_product_id] INT,
    [last_deposit_date] DATETIME,
    [last_win_date] DATETIME,
    [last_win_amount_gbp] DECIMAL(18, 2),
    [last_bonus_date] DATE,
    [bingo_bonus_balance] DECIMAL(18, 2),
    [bingo_loyalty_points_balance] INT,
    [bingo_pending_balance] DECIMAL(15, 2),
    [bingo_loyalty_level] VARCHAR(200),
    [bingo_date_level_achieved] DATETIME,
    [bingo_current_period_loyalty_points] INT,
    [activity_type_id] INT,
    [activity_status_id] INT,
    [total_vegas_complimentary_points] INT,
    [current_balance_complimentary_points] INT,
    [customer_lifestage_id] INT,
    [priority_first_product_id] INT,
    [priority_second_product_id] INT,
    [priority_third_product_id] INT,
    [priority_fourth_product_id] INT,
    [priority_fifth_product_id] INT,
    [priority_sixth_product_id] INT,
    [priority_first_sub_product_id] INT,
    [priority_second_sub_product_id] INT,
    [priority_third_sub_product_id] INT,
    [priority_fourth_sub_product_id] INT,
    [priority_fifth_sub_product_id] INT,
    [priority_sixth_sub_product_id] INT,
    [poker_reactivation_status] VARCHAR(30),
    [churn_prevention_model_flag] VARCHAR(1),
    [player_protection_model_flag] VARCHAR(1),
    [uvs_calculated_value] DECIMAL(10, 7),
    [uvs_tier_id] INT,
    [RequestId] INT,
    [SourceSystemId] INT,
    [PokerFirstActivityDt] DATE,
    [CasinoFirstActivityDt] DATE,
    [VegasFirstActivityDt] DATE,
    [GamesFirstActivityDt] DATE,
    [SkillFirstActivityDt] DATE,
    [LiveCasinoFirstActivityDt] DATE,
    [PokerSideGamesFirstActivityDt] DATE,
    [ScratchCardsGamesFirstActivityDt] DATE,
    [BetmostPokerFirstActivityDt] DATE,
    [BiGSLiCKPokerFirstActivityDt] DATE,
    [CDPokerFirstActivityDt] DATE,
    [SportsFirstActivityDt] DATE,
    [BingoFirstActivityDt] DATE,
    [tot_num_of_deposits] INT,
    [total_lifetime_value_deposits_gbp] DECIMAL(22, 6),
    [total_number_of_bets] INT,
    [total_lifetime_bet_value_gbp] MONEY,
    [total_active_days] INT,
    [bingosidegames_cust_acct_activity_type_id] INT,
    [total_number_of_withdrawals_gbp] INT,
    [bingo_cust_acct_activity_status_id] INT,
    [casino_cust_acct_activity_status_id] INT,
    [live_casino_cust_acct_activity_status_id] INT,
    [macau_cust_acct_activity_status_id] INT,
    [poker_cust_acct_activity_status_id] INT,
    [scratchcard_cust_acct_activity_status_id] INT,
    [sportsbook_cust_acct_activity_status_id] INT,
    [skill_cust_acct_activity_status_id] INT,
    [vegas_cust_acct_activity_status_id] INT,
    [MacauFirstActivityDt] DATE,
    [bingo_cust_acct_activity_type_id] INT,
    [casino_cust_acct_activity_type_id] INT,
    [live_casino_cust_acct_activity_type_id] INT,
    [macau_cust_acct_activity_type_id] INT,
    [poker_cust_acct_activity_type_id] INT,
    [scratchcard_cust_acct_activity_type_id] INT,
    [sportsbook_cust_acct_activity_type_id] INT,
    [skill_cust_acct_activity_type_id] INT,
    [vegas_cust_acct_activity_type_id] INT,
    [GamesCustomerAccountActivityStatusId] INT,
    [GamesCustomerAccountActivityStatusTypeId] INT,
    [last_deposit_amount_gbp] DECIMAL(18, 2),
    [last_withdrawal_amount_gbp] DECIMAL(18, 2),
    [last_bonus_amount_gbp] DECIMAL(18, 2),
    [BingoSideGamesFirstActivityDt] DATETIME,
    [DollarExchangeRate] REAL,
    [EuroExchangeRate] REAL,
    [KroneExchangeRate] REAL,
    [YenExchangeRate] REAL,
    [bingosidegames_cust_acct_activity_status_id] INT,
    [last_bet_amount_GBP] MONEY,
    [last_bet_date] DATETIME,
    [account_balance_gbp] MONEY,
    [total_lifetime_value_withdrawals_gbp] MONEY,
    [EligibilityFlag] CHAR(1),
    [EligibilityDate] DATETIME,
    [OptionInFlag] CHAR(1),
    [OptInDate] DATETIME,
    [MaximumStakefactor] DECIMAL(18, 4),
    [InPlayFlag] NCHAR(1),
    [AccaFlag] CHAR(1),
    [MobileFlag] CHAR(1),
    [TotalLifetimeActiveDaysOmni] CHAR(10),
    [TotalLifetimeBetsGBPOmni] MONEY,
    [TotalLifetimeDepositsGBPOmni] MONEY,
    [TotalLifetimeWithdrawalsGBPOmni] MONEY,
    [PriorityFirstChannelId] INT,
    [PrioritySecondChannelId] INT,
    [PriorityThirdChannelId] INT,
    [GraduationDt] DATE,
    [SportsFirstOmniActivityDt] DATE,
    [VegasFirstOmniActivityDt] DATE,
    [CasinoFirstOmniActivityDt] DATE,
    [LiveCasinoFirstOmniActivityDt] DATE,
    [GamesFirstOmniActivityDt] DATE,
    [last_ssbt_bet_date] DATE,
    [CustomerAccountOmniStatusId] INT,
    [CustomerAccountOmniLinkDt] DATE,
    [next_card_expiry_dt] DATE,
    [reactivation_status_sportsbook] CHAR(1),
    [reactivation_status_vegas] CHAR(1),
    [reactivation_status_macau] CHAR(1),
    [reactivation_status_casino] CHAR(1),
    [reactivation_status_live_casino] CHAR(1),
    [reactivation_status_games] CHAR(1),
    [reactivation_status_scratchcards] CHAR(1),
    [reactivation_status_bingo] CHAR(1),
    [reactivation_status_poker] CHAR(1),
    [customer_second_lifestage_id] INT,
    [churn_flag] CHAR(1),
    [customer_value_model_unified] INT,
    [customer_value_model_sportsbook] INT,
    [customer_value_model_gaming] INT,
    [LoyaltyPreRestrictedFlag] INT,
    [LoyaltyPreRestrictedDate] DATE,
    [LoyaltyRestrictedFlag] INT,
    [LoyaltyRestrictedDate] DATE,
    [LoyaltyBlockedFlag] INT,
    [LoyaltyBlockedDate] DATE,
    ----------------------------------------------------------------
    --- additional columns for https://jira.willhillatlas.com/browse/DANTF-810
    ----------------------------------------------------------------
    [CVMGamingRollingNew_PredictedChurnPreventionScore] VARCHAR(50),
    [CVMGamingRollingNew_PredictedGWPercGivenActive] VARCHAR(50),
    [CVMGamingRollingNew_PredictedNGRGivenActive] VARCHAR(50),
    [CVMGamingRollingNew_PredictedProbabilityActive] VARCHAR(50),
    [CVMGamingRollingNew_PredictedProbabilityCustomerWins] VARCHAR(50),
    [CVMGamingRollingNew_PredictedStakeGivenActive] VARCHAR(50),
    [CVMSportsbookRollingNew_PredictedChurnPreventionScore] VARCHAR(50),
    [CVMSportsbookRollingNew_PredictedGWPercGivenActive] VARCHAR(50),
    [CVMSportsbookRollingNew_PredictedNGRGivenActive] VARCHAR(50),
    [CVMSportsbookRollingNew_PredictedProbabilityActive] VARCHAR(50),
    [CVMSportsbookRollingNew_PredictedProbabilityCustomerWins] VARCHAR(50),
    [CVMSportsbookRollingNew_PredictedStakeGivenActive] VARCHAR(50),
    [GamingChurnPrevention_PredictedChurnPreventionScore] VARCHAR(50),
    [GamingChurnPrevention_PredictedGWPercGivenActive] VARCHAR(50),
    [GamingChurnPrevention_PredictedNGRGivenActive] VARCHAR(50),
    [GamingChurnPrevention_PredictedProbabilityActive] VARCHAR(50),
    [GamingChurnPrevention_PredictedProbabilityCustomerWins] VARCHAR(50),
    [GamingChurnPrevention_PredictedStakeGivenActive] VARCHAR(50)
)
;
