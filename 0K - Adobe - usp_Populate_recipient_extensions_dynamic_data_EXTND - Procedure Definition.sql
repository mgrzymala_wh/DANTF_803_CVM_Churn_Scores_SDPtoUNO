USE [StagingAdobeOutbound]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================================================================================  
-- Author:  WHGROUP\mgrzymala    
-- Create date: 2020-12-09   
-- Description: Create  [src].[usp_Populate_recipient_extensions_dynamic_data_EXTND]  
-- =======================================================================================================================================
/*
--to run:
TRUNCATE TABLE [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data_EXTND]
EXEC [StagingAdobeOutbound].[dbo].[usp_Populate_recipient_extensions_dynamic_data_EXTND]
--to test:
SELECT * FROM [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data_EXTND] ORDER BY global_account_id DESC
*/

-- =======================================================================================================================================  
-- Change History   
-- Date         User						Change                   
-- 2020-12-09   mgrzymala                   DANTF-810 - initial version
-- =======================================================================================================================================  

CREATE OR ALTER PROCEDURE [dbo].[usp_Populate_recipient_extensions_dynamic_data_EXTND]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET NOCOUNT ON;
SET XACT_ABORT ON;

TRUNCATE TABLE [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data_EXTND]
INSERT INTO [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data_EXTND]
(
global_account_id,
risk_profile_id,
viplevel_casino_id,
viplevel_poker_id,
viplevel_vegas_id,
account_balance,
last_login_date,
last_system_id,
last_withdrawal_date,
last_product_id,
last_deposit_date,
last_win_date,
last_win_amount_gbp,
last_bonus_date,
bingo_bonus_balance,
bingo_loyalty_points_balance,
bingo_pending_balance,
bingo_loyalty_level,
bingo_date_level_achieved,
bingo_current_period_loyalty_points,
activity_type_id,
activity_status_id,
total_vegas_complimentary_points,
current_balance_complimentary_points,
customer_lifestage_id,
priority_first_product_id,
priority_second_product_id,
priority_third_product_id,
priority_fourth_product_id,
priority_fifth_product_id,
priority_sixth_product_id,
priority_first_sub_product_id,
priority_second_sub_product_id,
priority_third_sub_product_id,
priority_fourth_sub_product_id,
priority_fifth_sub_product_id,
priority_sixth_sub_product_id,
poker_reactivation_status,
churn_prevention_model_flag,
player_protection_model_flag,
uvs_calculated_value,
uvs_tier_id,
RequestId,
SourceSystemId,
PokerFirstActivityDt,
CasinoFirstActivityDt,
VegasFirstActivityDt,
GamesFirstActivityDt,
SkillFirstActivityDt,
LiveCasinoFirstActivityDt,
PokerSideGamesFirstActivityDt,
ScratchCardsGamesFirstActivityDt,
BetmostPokerFirstActivityDt,
BiGSLiCKPokerFirstActivityDt,
CDPokerFirstActivityDt,
SportsFirstActivityDt,
BingoFirstActivityDt,
tot_num_of_deposits,
total_lifetime_value_deposits_gbp,
total_number_of_bets,
total_lifetime_bet_value_gbp,
total_active_days,
bingosidegames_cust_acct_activity_type_id,
total_number_of_withdrawals_gbp,
bingo_cust_acct_activity_status_id,
casino_cust_acct_activity_status_id,
live_casino_cust_acct_activity_status_id,
macau_cust_acct_activity_status_id,
poker_cust_acct_activity_status_id,
scratchcard_cust_acct_activity_status_id,
sportsbook_cust_acct_activity_status_id,
skill_cust_acct_activity_status_id,
vegas_cust_acct_activity_status_id,
MacauFirstActivityDt,
bingo_cust_acct_activity_type_id,
casino_cust_acct_activity_type_id,
live_casino_cust_acct_activity_type_id,
macau_cust_acct_activity_type_id,
poker_cust_acct_activity_type_id,
scratchcard_cust_acct_activity_type_id,
sportsbook_cust_acct_activity_type_id,
skill_cust_acct_activity_type_id,
vegas_cust_acct_activity_type_id,
GamesCustomerAccountActivityStatusId,
GamesCustomerAccountActivityStatusTypeId,
last_deposit_amount_gbp,
last_withdrawal_amount_gbp,
last_bonus_amount_gbp,
BingoSideGamesFirstActivityDt,
DollarExchangeRate,
EuroExchangeRate,
KroneExchangeRate,
YenExchangeRate,
bingosidegames_cust_acct_activity_status_id,
last_bet_amount_GBP,
last_bet_date,
account_balance_gbp,
total_lifetime_value_withdrawals_gbp,
EligibilityFlag,
EligibilityDate,
OptionInFlag,
OptInDate,
MaximumStakefactor,
InPlayFlag,
AccaFlag,
MobileFlag,
TotalLifetimeActiveDaysOmni,
TotalLifetimeBetsGBPOmni,
TotalLifetimeDepositsGBPOmni,
TotalLifetimeWithdrawalsGBPOmni,
PriorityFirstChannelId,
PrioritySecondChannelId,
PriorityThirdChannelId,
GraduationDt,
SportsFirstOmniActivityDt,
VegasFirstOmniActivityDt,
CasinoFirstOmniActivityDt,
LiveCasinoFirstOmniActivityDt,
GamesFirstOmniActivityDt,
last_ssbt_bet_date,
CustomerAccountOmniStatusId,
CustomerAccountOmniLinkDt,
next_card_expiry_dt,
reactivation_status_sportsbook,
reactivation_status_vegas,
reactivation_status_macau,
reactivation_status_casino,
reactivation_status_live_casino,
reactivation_status_games,
reactivation_status_scratchcards,
reactivation_status_bingo,
reactivation_status_poker,
customer_second_lifestage_id,
churn_flag,
customer_value_model_unified,
customer_value_model_sportsbook,
customer_value_model_gaming,
LoyaltyPreRestrictedFlag,
LoyaltyPreRestrictedDate,
LoyaltyRestrictedFlag,
LoyaltyRestrictedDate,
LoyaltyBlockedFlag,
LoyaltyBlockedDate,  
-----------------------------------------------------------------------------
CVMGamingRollingNew_PredictedChurnPreventionScore,
CVMGamingRollingNew_PredictedGWPercGivenActive,
CVMGamingRollingNew_PredictedNGRGivenActive,
CVMGamingRollingNew_PredictedProbabilityActive,
CVMGamingRollingNew_PredictedProbabilityCustomerWins,
CVMGamingRollingNew_PredictedStakeGivenActive,
CVMSportsbookRollingNew_PredictedChurnPreventionScore,
CVMSportsbookRollingNew_PredictedGWPercGivenActive,
CVMSportsbookRollingNew_PredictedNGRGivenActive,
CVMSportsbookRollingNew_PredictedProbabilityActive,
CVMSportsbookRollingNew_PredictedProbabilityCustomerWins,
CVMSportsbookRollingNew_PredictedStakeGivenActive,
GamingChurnPrevention_PredictedChurnPreventionScore,
GamingChurnPrevention_PredictedGWPercGivenActive,
GamingChurnPrevention_PredictedNGRGivenActive,
GamingChurnPrevention_PredictedProbabilityActive,
GamingChurnPrevention_PredictedProbabilityCustomerWins,
GamingChurnPrevention_PredictedStakeGivenActive
)

SELECT
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            redd.global_account_id,
            redd.risk_profile_id,
            redd.viplevel_casino_id,
            redd.viplevel_poker_id,
            redd.viplevel_vegas_id,
            redd.account_balance,
            redd.last_login_date,
            redd.last_system_id,
            redd.last_withdrawal_date,
            redd.last_product_id,
            redd.last_deposit_date,
            redd.last_win_date,
            redd.last_win_amount_gbp,
            redd.last_bonus_date,
            redd.bingo_bonus_balance,
            redd.bingo_loyalty_points_balance,
            redd.bingo_pending_balance,
            redd.bingo_loyalty_level,
            redd.bingo_date_level_achieved,
            redd.bingo_current_period_loyalty_points,
            redd.activity_type_id,
            redd.activity_status_id,
            redd.total_vegas_complimentary_points,
            redd.current_balance_complimentary_points,
            redd.customer_lifestage_id,
            redd.priority_first_product_id,
            redd.priority_second_product_id,
            redd.priority_third_product_id,
            redd.priority_fourth_product_id,
            redd.priority_fifth_product_id,
            redd.priority_sixth_product_id,
            redd.priority_first_sub_product_id,
            redd.priority_second_sub_product_id,
            redd.priority_third_sub_product_id,
            redd.priority_fourth_sub_product_id,
            redd.priority_fifth_sub_product_id,
            redd.priority_sixth_sub_product_id,
            redd.poker_reactivation_status,
            redd.churn_prevention_model_flag,
            redd.player_protection_model_flag,
            redd.uvs_calculated_value,
            redd.uvs_tier_id,
            redd.RequestId,
            redd.SourceSystemId,
            redd.PokerFirstActivityDt,
            redd.CasinoFirstActivityDt,
            redd.VegasFirstActivityDt,
            redd.GamesFirstActivityDt,
            redd.SkillFirstActivityDt,
            redd.LiveCasinoFirstActivityDt,
            redd.PokerSideGamesFirstActivityDt,
            redd.ScratchCardsGamesFirstActivityDt,
            redd.BetmostPokerFirstActivityDt,
            redd.BiGSLiCKPokerFirstActivityDt,
            redd.CDPokerFirstActivityDt,
            redd.SportsFirstActivityDt,
            redd.BingoFirstActivityDt,
            redd.tot_num_of_deposits,
            redd.total_lifetime_value_deposits_gbp,
            redd.total_number_of_bets,
            redd.total_lifetime_bet_value_gbp,
            redd.total_active_days,
            redd.bingosidegames_cust_acct_activity_type_id,
            redd.total_number_of_withdrawals_gbp,
            redd.bingo_cust_acct_activity_status_id,
            redd.casino_cust_acct_activity_status_id,
            redd.live_casino_cust_acct_activity_status_id,
            redd.macau_cust_acct_activity_status_id,
            redd.poker_cust_acct_activity_status_id,
            redd.scratchcard_cust_acct_activity_status_id,
            redd.sportsbook_cust_acct_activity_status_id,
            redd.skill_cust_acct_activity_status_id,
            redd.vegas_cust_acct_activity_status_id,
            redd.MacauFirstActivityDt,
            redd.bingo_cust_acct_activity_type_id,
            redd.casino_cust_acct_activity_type_id,
            redd.live_casino_cust_acct_activity_type_id,
            redd.macau_cust_acct_activity_type_id,
            redd.poker_cust_acct_activity_type_id,
            redd.scratchcard_cust_acct_activity_type_id,
            redd.sportsbook_cust_acct_activity_type_id,
            redd.skill_cust_acct_activity_type_id,
            redd.vegas_cust_acct_activity_type_id,
            redd.GamesCustomerAccountActivityStatusId,
            redd.GamesCustomerAccountActivityStatusTypeId,
            redd.last_deposit_amount_gbp,
            redd.last_withdrawal_amount_gbp,
            redd.last_bonus_amount_gbp,
            redd.BingoSideGamesFirstActivityDt,
            redd.DollarExchangeRate,
            redd.EuroExchangeRate,
            redd.KroneExchangeRate,
            redd.YenExchangeRate,
            redd.bingosidegames_cust_acct_activity_status_id,
            redd.last_bet_amount_GBP,
            redd.last_bet_date,
            redd.account_balance_gbp,
            redd.total_lifetime_value_withdrawals_gbp,
            redd.EligibilityFlag,
            redd.EligibilityDate,
            redd.OptionInFlag,
            redd.OptInDate,
            redd.MaximumStakefactor,
            redd.InPlayFlag,
            redd.AccaFlag,
            redd.MobileFlag,
            redd.TotalLifetimeActiveDaysOmni,
            redd.TotalLifetimeBetsGBPOmni,
            redd.TotalLifetimeDepositsGBPOmni,
            redd.TotalLifetimeWithdrawalsGBPOmni,
            redd.PriorityFirstChannelId,
            redd.PrioritySecondChannelId,
            redd.PriorityThirdChannelId,
            redd.GraduationDt,
            redd.SportsFirstOmniActivityDt,
            redd.VegasFirstOmniActivityDt,
            redd.CasinoFirstOmniActivityDt,
            redd.LiveCasinoFirstOmniActivityDt,
            redd.GamesFirstOmniActivityDt,
            redd.last_ssbt_bet_date,
            redd.CustomerAccountOmniStatusId,
            redd.CustomerAccountOmniLinkDt,
            redd.next_card_expiry_dt,
            redd.reactivation_status_sportsbook,
            redd.reactivation_status_vegas,
            redd.reactivation_status_macau,
            redd.reactivation_status_casino,
            redd.reactivation_status_live_casino,
            redd.reactivation_status_games,
            redd.reactivation_status_scratchcards,
            redd.reactivation_status_bingo,
            redd.reactivation_status_poker,
            redd.customer_second_lifestage_id,
            redd.churn_flag,
            redd.customer_value_model_unified,
            redd.customer_value_model_sportsbook,
            redd.customer_value_model_gaming,
            redd.LoyaltyPreRestrictedFlag,
            redd.LoyaltyPreRestrictedDate,
            redd.LoyaltyRestrictedFlag,
            redd.LoyaltyRestrictedDate,
            redd.LoyaltyBlockedFlag,
            redd.LoyaltyBlockedDate,  
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
              fn.CVMGamingRollingNew_PredictedChurnPreventionScore,
              fn.CVMGamingRollingNew_PredictedGWPercGivenActive,
              fn.CVMGamingRollingNew_PredictedNGRGivenActive,
              fn.CVMGamingRollingNew_PredictedProbabilityActive,
              fn.CVMGamingRollingNew_PredictedProbabilityCustomerWins,
              fn.CVMGamingRollingNew_PredictedStakeGivenActive,
              fn.CVMSportsbookRollingNew_PredictedChurnPreventionScore,
              fn.CVMSportsbookRollingNew_PredictedGWPercGivenActive,
              fn.CVMSportsbookRollingNew_PredictedNGRGivenActive,
              fn.CVMSportsbookRollingNew_PredictedProbabilityActive,
              fn.CVMSportsbookRollingNew_PredictedProbabilityCustomerWins,
              fn.CVMSportsbookRollingNew_PredictedStakeGivenActive,
              fn.GamingChurnPrevention_PredictedChurnPreventionScore,
              fn.GamingChurnPrevention_PredictedGWPercGivenActive,
              fn.GamingChurnPrevention_PredictedNGRGivenActive,
              fn.GamingChurnPrevention_PredictedProbabilityActive,
              fn.GamingChurnPrevention_PredictedProbabilityCustomerWins,
              fn.GamingChurnPrevention_PredictedStakeGivenActive

-- 1. if we want to include records that exist BOTH in our pivot AND in recipient_extensions_dynamic_data:
--FROM [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data] redd
--CROSS APPLY [BusinessSemanticLayer].[MRG].[ufn_GetRModelDataFlags_Pivoted] (redd.[global_account_id]) fn

-- 2. if we want to include records that exist in our pivot OR (optionally) in recipient_extensions_dynamic_data:
--FROM [MRG].[RModelDataFlags_Pivoted_All] fn 
--LEFT JOIN [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data] redd ON fn.CustomerAccountId = redd.[global_account_id]

-- 3. safest option: if we want to include all records that exist recipient_extensions_dynamic_data OR exist (optionally) in our pivot table:
FROM [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data] redd   
LEFT JOIN [BusinessSemanticLayer].[MRG].[RModelDataFlags_Pivoted_All] fn ON fn.[CustomerAccountId] = redd.[global_account_id]

-- below predicate to be removed (this is for testing only)
/*
WHERE 

   fn.CVMGamingRollingNew_PredictedChurnPreventionScore IS NOT NULL
OR fn.CVMGamingRollingNew_PredictedGWPercGivenActive IS NOT NULL
OR fn.CVMGamingRollingNew_PredictedNGRGivenActive IS NOT NULL
OR fn.CVMGamingRollingNew_PredictedProbabilityActive IS NOT NULL
OR fn.CVMGamingRollingNew_PredictedProbabilityCustomerWins IS NOT NULL
OR fn.CVMGamingRollingNew_PredictedStakeGivenActive IS NOT NULL
OR fn.CVMSportsbookRollingNew_PredictedChurnPreventionScore IS NOT NULL
OR fn.CVMSportsbookRollingNew_PredictedGWPercGivenActive IS NOT NULL
OR fn.CVMSportsbookRollingNew_PredictedNGRGivenActive IS NOT NULL
OR fn.CVMSportsbookRollingNew_PredictedProbabilityActive IS NOT NULL
OR fn.CVMSportsbookRollingNew_PredictedProbabilityCustomerWins IS NOT NULL
OR fn.CVMSportsbookRollingNew_PredictedStakeGivenActive IS NOT NULL
OR fn.GamingChurnPrevention_PredictedChurnPreventionScore IS NOT NULL
OR fn.GamingChurnPrevention_PredictedGWPercGivenActive IS NOT NULL
OR fn.GamingChurnPrevention_PredictedNGRGivenActive IS NOT NULL
OR fn.GamingChurnPrevention_PredictedProbabilityActive IS NOT NULL
OR fn.GamingChurnPrevention_PredictedProbabilityCustomerWins IS NOT NULL
OR fn.GamingChurnPrevention_PredictedStakeGivenActive IS NOT NULL
*/
END
