USE [BusinessSemanticLayer]
GO


CREATE OR ALTER FUNCTION MRG.ufn_GetRModelDataFlags_Pivoted (@CustomerAccountIdID AS INT)
RETURNS TABLE
AS
  RETURN
  (
        SELECT 
                 pva.[CustomerAccountId]                                       
                ,pva.[CVMGamingRollingNew_PredictedChurnPreventionScore]       
                ,pva.[CVMGamingRollingNew_PredictedGWPercGivenActive]          
                ,pva.[CVMGamingRollingNew_PredictedNGRGivenActive]             
                ,pva.[CVMGamingRollingNew_PredictedProbabilityActive]          
                ,pva.[CVMGamingRollingNew_PredictedProbabilityCustomerWins]    
                ,pva.[CVMGamingRollingNew_PredictedStakeGivenActive]           
                ,pva.[CVMSportsbookRollingNew_PredictedChurnPreventionScore]   
                ,pva.[CVMSportsbookRollingNew_PredictedGWPercGivenActive]      
                ,pva.[CVMSportsbookRollingNew_PredictedNGRGivenActive]         
                ,pva.[CVMSportsbookRollingNew_PredictedProbabilityActive]      
                ,pva.[CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]
                ,pva.[CVMSportsbookRollingNew_PredictedStakeGivenActive]       
                ,pva.[GamingChurnPrevention_PredictedChurnPreventionScore]     
                ,pva.[GamingChurnPrevention_PredictedGWPercGivenActive]        
                ,pva.[GamingChurnPrevention_PredictedNGRGivenActive]           
                ,pva.[GamingChurnPrevention_PredictedProbabilityActive]        
                ,pva.[GamingChurnPrevention_PredictedProbabilityCustomerWins]  
                ,pva.[GamingChurnPrevention_PredictedStakeGivenActive]                  

        FROM [MRG].[RModelDataFlags_Pivoted_All] pva
        WHERE pva.CustomerAccountId = @CustomerAccountIdID
  )
GO
