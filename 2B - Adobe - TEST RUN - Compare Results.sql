USE [BusinessSemanticLayer]
GO

SELECT COUNT(*) FROM MRG.RModelDataFlags

--------------------------------------------------------------------------------------------------------
-- �����������������������������������������������������������������������������������������������������
--------------------------------------------------------------------------------------------------------
SELECT      
            CAPV.CustomerAccountProfileValId,
            CAPV.CustomerAccountId,
            CAPV.CustomerAccountProfileTypeId,
            CAPT.CustomerAccountProfileTypeCd,
            CAPV.CustomerAccountProfileMeasureId,
            CAPM.CustomerAccountProfileMeasureCd,
            CAPV.CustomerAccountValNumber,
            CAPV.CustomerAccountValTxt,
            CAPV.InformationSourceId,
            CAPV.SystemCreatedDt,
            CAPV.UpdateByStaffId,
            CAPV.SystemUpdatedDt
            --COUNT(CustomerAccountId) AS [Count]
FROM        dbo.CustomerAccountProfileVal CAPV
LEFT JOIN   dbo.CustomerAccountProfileType AS CAPT ON CAPT.CustomerAccountProfileTypeId = CAPV.CustomerAccountProfileTypeId
LEFT JOIN   dbo.CustomerAccountProfileMeasure AS CAPM ON CAPM.CustomerAccountProfileMeasureId = CAPV.CustomerAccountProfileMeasureId
WHERE       CAPV.SystemUpdatedDt > GETDATE() -1 
AND         CAPV.CustomerAccountId IN (40161111, 40341300, 40261053, 39464481)
--GROUP BY  CustomerAccountId, CustomerAccountProfileMeasureId
ORDER BY    CAPV.CustomerAccountId, CAPV.CustomerAccountProfileTypeId, CAPV.CustomerAccountProfileMeasureId, CAPV.CustomerAccountValNumber DESC
-------------------------------------------------------------------------------------------------------
SELECT      
            rmdf.CustomerAccountId,
            CAPT.CustomerAccountProfileTypeId,
            rmdf.CustomerAccountProfileTypeCd,
            CAPM.CustomerAccountProfileMeasureId,
            rmdf.CustomerAccountProfileMeasureCd,
            rmdf.CustomerAccountValTxt

FROM        MRG.RModelDataFlags rmdf
LEFT JOIN   dbo.CustomerAccountProfileType AS CAPT ON CAPT.CustomerAccountProfileTypeCd = rmdf.CustomerAccountProfileTypeCd
LEFT JOIN   dbo.CustomerAccountProfileMeasure AS CAPM ON CAPM.CustomerAccountProfileMeasureCd = rmdf.CustomerAccountProfileMeasureCd
WHERE       1 = 1
--AND         CustomerAccountProfileTypeCd IN ( 'GamingChurnPrevention', 'CVMSportsbookRollingNew', 'CVMGamingRollingNew')
AND         rmdf.CustomerAccountId IN (40161111, 40341300, 40261053, 39464481)
ORDER BY    rmdf.CustomerAccountId, CAPT.CustomerAccountProfileTypeId, CAPM.CustomerAccountProfileMeasureId
--------------------------------------------------------------------------------------------------------
-- �����������������������������������������������������������������������������������������������������
--------------------------------------------------------------------------------------------------------
SELECT      CAPV.* 
--UPDATE      CAPV
--SET         CAPV.CustomerAccountProfileTypeId = 104119, CAPV.CustomerAccountProfileMeasureId = 103599, CAPV.CustomerAccountValNumber = 1021.75, CAPV.CustomerAccountValTxt = '102175.00'
FROM        dbo.CustomerAccountProfileVal CAPV
LEFT JOIN   dbo.CustomerAccountProfileType AS CAPT ON CAPT.CustomerAccountProfileTypeId = CAPV.CustomerAccountProfileTypeId
LEFT JOIN   dbo.CustomerAccountProfileMeasure AS CAPM ON CAPM.CustomerAccountProfileMeasureId = CAPV.CustomerAccountProfileMeasureId
WHERE       1 = 1 
AND         CAPV.CustomerAccountProfileValId =  14163834029
AND         CAPV.SystemUpdatedDt > GETDATE() -1 
AND         CAPV.CustomerAccountId = 40161111 --40341300, --40261053, --39464481 


SELECT TOP 100 *, CAST(CAPV.CustomerAccountValNumber * 100 AS VARCHAR(50)) 
--UPDATE      CAPV
--SET         CAPV.CustomerAccountValTxt = CAST(CAPV.CustomerAccountValNumber * 100 AS VARCHAR(50)) 
FROM        dbo.CustomerAccountProfileVal CAPV 
WHERE       1 = 1
AND         CAPV.SystemUpdatedDt > GETDATE() -1 
AND         CAPV.CustomerAccountValTxt IS NULL
--------------------------------------------------------------------------------------------------------
-- �����������������������������������������������������������������������������������������������������
--------------------------------------------------------------------------------------------------------

SELECT DISTINCT capm.CustomerAccountProfileMeasureId,
                rmf.CustomerAccountProfileMeasureCd
FROM            MRG.RModelDataFlags rmf
INNER JOIN      dbo.CustomerAccountProfileMeasure AS capm ON capm.CustomerAccountProfileMeasureCd = rmf.CustomerAccountProfileMeasureCd
/*
103598	PredictedProbabilityActive
103599	PredictedProbabilityCustomerWins
103600	PredictedNGRGivenActive
103601	PredictedGWPercGivenActive
103602	PredictedStakeGivenActive
103603	PredictedChurnPreventionScore
*/
SELECT DISTINCT capt.CustomerAccountProfileTypeId,
                rmf.CustomerAccountProfileTypeCd 
FROM            MRG.RModelDataFlags rmf
INNER JOIN      dbo.CustomerAccountProfileType AS capt ON capt.CustomerAccountProfiletypeCd = rmf.CustomerAccountProfiletypeCd
/*
104118	CVMGamingRollingNew
104119	CVMSportsbookRollingNew
104120	GamingChurnPrevention
*/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT    
         CustomerAccountId
         --CustomerAccountValTxt,
         --COUNT(CustomerAccountProfileMeasureCd) AS [MeasureCount],
         --,COUNT(CustomerAccountProfileTypeCd) AS [TypeCount]
FROM     MRG.RModelDataFlags
WHERE    CustomerAccountId IN (40161111, 40341300, 40261053, 39464481)
GROUP BY CustomerAccountId
HAVING   COUNT(DISTINCT CustomerAccountProfileTypeCd) >= 3
--ORDER BY 1
INTERSECT
SELECT    
         CustomerAccountId
         --CustomerAccountValTxt,
         --,COUNT(DISTINCT CustomerAccountProfileMeasureCd) AS [MeasureCount]
         --COUNT(CustomerAccountProfileTypeCd) AS [TypeCount]
FROM     MRG.RModelDataFlags
WHERE    CustomerAccountId IN (40161111, 40341300, 40261053, 39464481)
GROUP BY CustomerAccountId
HAVING   COUNT(CustomerAccountProfileMeasureCd) >= 2
ORDER BY 1
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT CustomerAccountId,
       CustomerAccountProfileTypeCd,
       --CustomerAccountProfileMeasureCd,
       COUNT(CustomerAccountId) AS [Count]
FROM MRG.RModelDataFlags
WHERE CustomerAccountProfileTypeCd IN ( 'GamingChurnPrevention', 'CVMSportsbookRollingNew', 'CVMGamingRollingNew')
--WHERE CustomerAccountProfileMeasureCd IN ('PredictedNGRGivenActive', 'PredictedChurnPreventionScore', 'PredictedStakeGivenActive', 'PredictedProbabilityCustomerWins', 'PredictedProbabilityActive', 'PredictedGWPercGivenActive')
AND CustomerAccountId IN (40161111, 40341300, 40261053, 39464481)
GROUP BY CustomerAccountId
, CustomerAccountProfileTypeCd
--, CustomerAccountProfileMeasureCd 
--HAVING COUNT(CustomerAccountId) > 1
ORDER BY 1, 2, 3 DESC

SELECT CustomerAccountId,
       --CustomerAccountProfileTypeCd,
       CustomerAccountProfileMeasureCd,
       COUNT(CustomerAccountId) AS [Count]
FROM MRG.RModelDataFlags
--WHERE CustomerAccountProfileTypeCd IN ( 'GamingChurnPrevention', 'CVMSportsbookRollingNew', 'CVMGamingRollingNew')
WHERE CustomerAccountProfileMeasureCd IN ('PredictedNGRGivenActive', 'PredictedChurnPreventionScore', 'PredictedStakeGivenActive', 'PredictedProbabilityCustomerWins', 'PredictedProbabilityActive', 'PredictedGWPercGivenActive')
AND CustomerAccountId IN (40161111, 40341300, 40261053, 39464481)
GROUP BY CustomerAccountId
--, CustomerAccountProfileTypeCd
, CustomerAccountProfileMeasureCd 
--HAVING COUNT(CustomerAccountId) > 1
ORDER BY 1, 2, 3 DESC 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------