USE [BusinessSemanticLayer]
GO
/*
CHECKPOINT;
GO

DBCC DROPCLEANBUFFERS;
GO

DBCC FREEPROCCACHE;
GO

DBCC FREESYSTEMCACHE ('ALL');
GO

DBCC FREESESSIONCACHE
GO

SET STATISTICS TIME, IO ON
GO
*/

-- Dropping tables
DROP TABLE IF EXISTS #tempCustLists;
DROP TABLE IF EXISTS #CustOptInDate;
DROP TABLE IF EXISTS #MobileFlag;
DROP TABLE IF EXISTS #AccaFlag;
DROP TABLE IF EXISTS #InPlayFlag;
DROP TABLE IF EXISTS #mindates;
DROP TABLE IF EXISTS #omnidates;
DROP TABLE IF EXISTS #OminCustomers;
DROP TABLE IF EXISTS #last_SSBT_bet;
DROP TABLE IF EXISTS #Exp_Omni_card;
DROP TABLE IF EXISTS #FlagProfile;
DROP TABLE IF EXISTS #SportsbookUnpivot;
DROP TABLE IF EXISTS #GamingUnpivot;
DROP TABLE IF EXISTS #Unified;
DROP TABLE IF EXISTS #ReactivationProductFlags;
DROP TABLE IF EXISTS #DomainCategory;
DROP TABLE IF EXISTS #dailyBettingAggregate;
DROP TABLE IF EXISTS #dailyDepositAggregate;


DECLARE 
    @StartDate DATETIME = GETDATE() - 90, --'2000-01-01 00:00:00.000', --
    @EndDate DATETIME = GETDATE() + 30
------------------------------------------------------------------------------------------------------------------------------------
     CREATE table #DomainCategory
            (
              ChannelId INT 
            );

        INSERT  INTO #DomainCategory
                ( ChannelId 
                )
                SELECT DISTINCT
                        ChannelId 
                FROM    [dbo].[Channel] C
                        INNER JOIN dbo.Domain D ON C.DomainId = D.DomainId
                        INNER JOIN [dbo].[DomainCategory] DC ON D.[DomainCategoryId] = DC.[DomainCategoryId]
                WHERE   DC.[DomainCategoryCd] NOT IN ( 'US' );

WITH custs
  AS (
      SELECT ca.CustomerAccountId
        FROM dbo.CustomerAccount AS ca (NOLOCK)
       WHERE ca.SystemUpdatedDt > @StartDate
         AND ca.SystemUpdatedDt <= @EndDate
      UNION
      SELECT cap.CustomerAccountId
        FROM dbo.CustomerAccountProfile AS cap (NOLOCK)
       WHERE cap.SystemUpdatedDt > @StartDate
         AND cap.SystemUpdatedDt <= @EndDate
      UNION
      SELECT cafp.CustomerAccountId
        FROM dbo.CustomerAccountFlagProfile AS cafp (NOLOCK)
       WHERE cafp.SystemUpdatedDt > @StartDate
         AND cafp.SystemUpdatedDt <= @EndDate
      UNION
      SELECT DISTINCT q1.CustomerAccountId
        FROM MRG.CustomerSourceBettingDailyAggregateRolling (NOLOCK) q1
       WHERE q1.SystemCreatedDt > @StartDate
         AND q1.SystemCreatedDt <=@EndDate
      UNION
      SELECT DISTINCT q1.CustomerAccountId
        FROM MRG.CustomerSourceDepositDailyAggregateRolling (NOLOCK) q1
       WHERE q1.SystemCreatedDt > @StartDate
         AND q1.SystemCreatedDt <= @EndDate
	  UNION
      SELECT DISTINCT capv.CustomerAccountId
        FROM dbo.CustomerAccountProfileVal AS capv (NOLOCK)
       WHERE capv.SystemUpdatedDt > @StartDate
         AND capv.SystemUpdatedDt <= @EndDate
	  UNION
      SELECT DISTINCT cadp.CustomerAccountId
        FROM dbo.CustomerAccountDerivedProfile AS cadp (NOLOCK)
       WHERE cadp.SystemUpdatedDt > @StartDate
         AND cadp.SystemUpdatedDt <= @EndDate
	  UNION
      SELECT DISTINCT CAPR.CustomerAccountId
        FROM dbo.CustomerAccountProductReactivation AS CAPR (NOLOCK)
       WHERE CAPR.SystemUpdatedDt > @StartDate
         AND CAPR.SystemUpdatedDt <= @EndDate
	  UNION
      SELECT DISTINCT pc.CustomerAccountId
        FROM dbo.PaymentCard AS pc (NOLOCK)
       --WHERE pc.SystemUpdatedDt > @StartDate
       --  AND pc.SystemUpdatedDt <= @EndDate		 
		 
		 )
SELECT          custs.CustomerAccountId
INTO            #tempCustLists
FROM            custs
INNER JOIN      dbo.vwCustomerAccount (NOLOCK) AS vca
INNER JOIN      #DomainCategory AS dc ON dc.ChannelId = vca.RegistrationChannelId  -- TFS 20541 - Adding filter of RTBF and US data to not send to Adobe
INNER JOIN      dbo.CustomerAccountFlagProfile AS cafp ON cafp.CustomerAccountId = vca.CustomerAccountId  -- TFS 20541 - Adding filter of RTBF and US data to not send to Adobe
ON              vca.CustomerAccountId = custs.CustomerAccountId
AND             vca.IntegratedAccountFlag='Y'
AND             cafp.RightToBeForgottenFlag = 'N'	;  --  TFS 20541 - Adding filter of RTBF and US data to not send to Adobe

--SELECT COUNT(*) FROM #tempCustLists

------------------------------------------------START TFS 16162---------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------
--Create a dataset from CustomerAccountProfileVal that contains only the latest value of each combination of Type and Measure for each customerid
TRUNCATE TABLE  MRG.RModelDataFlags
INSERT INTO     MRG.RModelDataFlags
        ( CustomerAccountId ,
          CustomerAccountValTxt ,
		  CustomerAccountProfileTypeCd,
          CustomerAccountProfileMeasureCd

        )
        SELECT  RMC.CustomerAccountId ,
                RMC.CustomerAccountValTxt ,
				RMC.CustomerAccountProfileTypeCd,
                RMC.CustomerAccountProfileMeasureCd

        FROM    ( SELECT    CAPV.CustomerAccountId ,
                            CAPV.CustomerAccountValTxt,
							CAPT.CustomerAccountProfileTypeCd,
                            CAPM.CustomerAccountProfileMeasureCd,

                            RN = ROW_NUMBER() OVER ( PARTITION BY CAPV.CustomerAccountId, CAPV.CustomerAccountProfileTypeId, CAPV.CustomerAccountProfileMeasureId 
                                                     ORDER BY     CAPV.SystemUpdatedDt DESC
                                                                , CAPV.CustomerAccountValNumber DESC-- <== if we have more than one value of with the same Type, Measure and SystemUpdatedDt combination then let's take the largest one
                                                     ) 
                  FROM      dbo.CustomerAccountProfileVal AS CAPV 
							INNER JOIN #tempCustLists AS TCL ON TCL.CustomerAccountId = CAPV.CustomerAccountId
                            INNER JOIN dbo.CustomerAccountProfileType AS CAPT ON CAPT.CustomerAccountProfileTypeId = CAPV.CustomerAccountProfileTypeId
                            INNER JOIN dbo.CustomerAccountProfileMeasure AS CAPM ON CAPM.CustomerAccountProfileMeasureId = CAPV.CustomerAccountProfileMeasureId
                  WHERE     CAPV.SystemUpdatedDt >= @StartDate
				  AND       CAPV.SystemUpdatedDt < @EndDate
                ) RMC
        WHERE   RMC.RN = 1;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CustomerAccountProfileTypeNm:  CVMGamingRollingNew:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--SELECT COUNT(*) FROM  dbo.CustomerAccountProfileVal WHERE CustomerAccountProfileTypeId IS NOT NULL AND CustomerAccountProfileMeasureId IS NOT NULL

DECLARE @_CustomerAccountProfileMeasureCds AS [MRG].[CustomerAccountProfileMeasureCds]
INSERT INTO @_CustomerAccountProfileMeasureCds VALUES 
 ('PredictedChurnPreventionScore')
,('PredictedGWPercGivenActive')
,('PredictedNGRGivenActive')
,('PredictedProbabilityActive')
,('PredictedProbabilityCustomerWins')
,('PredictedStakeGivenActive')

TRUNCATE TABLE  [MRG].[RModelDataFlags_Pivoted_CVMGamingRollingNew]
INSERT INTO     [MRG].[RModelDataFlags_Pivoted_CVMGamingRollingNew] (
                [CustomerAccountId]
               ,[CVMGamingRollingNew_PredictedChurnPreventionScore]
               ,[CVMGamingRollingNew_PredictedGWPercGivenActive]
               ,[CVMGamingRollingNew_PredictedNGRGivenActive]
               ,[CVMGamingRollingNew_PredictedProbabilityActive]
               ,[CVMGamingRollingNew_PredictedProbabilityCustomerWins]
               ,[CVMGamingRollingNew_PredictedStakeGivenActive]
)
EXEC            [MRG].[usp_Populate_RModelDataFlags_Pivoted_Tables] @CustomerAccountProfileTypeCd = 'CVMGamingRollingNew', @CustomerAccountProfileMeasureCds = @_CustomerAccountProfileMeasureCds
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CustomerAccountProfileTypeNm:  CVMSportsbookRollingNew:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE  [MRG].[RModelDataFlags_Pivoted_CVMSportsbookRollingNew]
INSERT INTO     [MRG].[RModelDataFlags_Pivoted_CVMSportsbookRollingNew] (
                [CustomerAccountId]
               ,[CVMSportsbookRollingNew_PredictedChurnPreventionScore]
               ,[CVMSportsbookRollingNew_PredictedGWPercGivenActive]
               ,[CVMSportsbookRollingNew_PredictedNGRGivenActive]
               ,[CVMSportsbookRollingNew_PredictedProbabilityActive]
               ,[CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]
               ,[CVMSportsbookRollingNew_PredictedStakeGivenActive]
)
EXEC            [MRG].[usp_Populate_RModelDataFlags_Pivoted_Tables] @CustomerAccountProfileTypeCd = 'CVMSportsbookRollingNew', @CustomerAccountProfileMeasureCds = @_CustomerAccountProfileMeasureCds
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CustomerAccountProfileTypeNm:  GamingChurnPrevention:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE  [MRG].[RModelDataFlags_Pivoted_GamingChurnPrevention]
INSERT INTO     [MRG].[RModelDataFlags_Pivoted_GamingChurnPrevention] (
                [CustomerAccountId]
               ,[GamingChurnPrevention_PredictedChurnPreventionScore]
               ,[GamingChurnPrevention_PredictedGWPercGivenActive]
               ,[GamingChurnPrevention_PredictedNGRGivenActive]
               ,[GamingChurnPrevention_PredictedProbabilityActive]
               ,[GamingChurnPrevention_PredictedProbabilityCustomerWins]
               ,[GamingChurnPrevention_PredictedStakeGivenActive]
)
EXEC            [MRG].[usp_Populate_RModelDataFlags_Pivoted_Tables] @CustomerAccountProfileTypeCd = 'GamingChurnPrevention', @CustomerAccountProfileMeasureCds = @_CustomerAccountProfileMeasureCds
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     JOINING PIVOTS FOR ALL 3 TYPES:    $    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TRUNCATE TABLE   [MRG].[RModelDataFlags_Pivoted_All]
INSERT INTO      [MRG].[RModelDataFlags_Pivoted_All] (
                 [CustomerAccountId]                                       
                ,[CVMGamingRollingNew_PredictedChurnPreventionScore]       
                ,[CVMGamingRollingNew_PredictedGWPercGivenActive]          
                ,[CVMGamingRollingNew_PredictedNGRGivenActive]             
                ,[CVMGamingRollingNew_PredictedProbabilityActive]          
                ,[CVMGamingRollingNew_PredictedProbabilityCustomerWins]    
                ,[CVMGamingRollingNew_PredictedStakeGivenActive]           
                ,[CVMSportsbookRollingNew_PredictedChurnPreventionScore]   
                ,[CVMSportsbookRollingNew_PredictedGWPercGivenActive]      
                ,[CVMSportsbookRollingNew_PredictedNGRGivenActive]         
                ,[CVMSportsbookRollingNew_PredictedProbabilityActive]      
                ,[CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]
                ,[CVMSportsbookRollingNew_PredictedStakeGivenActive]       
                ,[GamingChurnPrevention_PredictedChurnPreventionScore]     
                ,[GamingChurnPrevention_PredictedGWPercGivenActive]        
                ,[GamingChurnPrevention_PredictedNGRGivenActive]           
                ,[GamingChurnPrevention_PredictedProbabilityActive]        
                ,[GamingChurnPrevention_PredictedProbabilityCustomerWins]  
                ,[GamingChurnPrevention_PredictedStakeGivenActive]         
)

SELECT          DISTINCT 
                rmdf.CustomerAccountId
               ,grn.[CVMGamingRollingNew_PredictedChurnPreventionScore]
               ,grn.[CVMGamingRollingNew_PredictedGWPercGivenActive]
               ,grn.[CVMGamingRollingNew_PredictedNGRGivenActive]
               ,grn.[CVMGamingRollingNew_PredictedProbabilityActive]
               ,grn.[CVMGamingRollingNew_PredictedProbabilityCustomerWins]
               ,grn.[CVMGamingRollingNew_PredictedStakeGivenActive]
               
               ,srn.[CVMSportsbookRollingNew_PredictedChurnPreventionScore]
               ,srn.[CVMSportsbookRollingNew_PredictedGWPercGivenActive]
               ,srn.[CVMSportsbookRollingNew_PredictedNGRGivenActive]
               ,srn.[CVMSportsbookRollingNew_PredictedProbabilityActive]
               ,srn.[CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]
               ,srn.[CVMSportsbookRollingNew_PredictedStakeGivenActive]
               
               ,gcp.[GamingChurnPrevention_PredictedChurnPreventionScore]
               ,gcp.[GamingChurnPrevention_PredictedGWPercGivenActive]
               ,gcp.[GamingChurnPrevention_PredictedNGRGivenActive]
               ,gcp.[GamingChurnPrevention_PredictedProbabilityActive]
               ,gcp.[GamingChurnPrevention_PredictedProbabilityCustomerWins]
               ,gcp.[GamingChurnPrevention_PredictedStakeGivenActive]

FROM            MRG.RModelDataFlags rmdf
LEFT JOIN       MRG.RModelDataFlags_Pivoted_CVMGamingRollingNew      grn ON grn.CustomerAccountId = rmdf.CustomerAccountId 
LEFT JOIN       MRG.RModelDataFlags_Pivoted_CVMSportsbookRollingNew  srn ON srn.CustomerAccountId = rmdf.CustomerAccountId
LEFT JOIN       MRG.RModelDataFlags_Pivoted_GamingChurnPrevention    gcp ON gcp.CustomerAccountId = rmdf.CustomerAccountId

--WHERE rmdf.CustomerAccountId IN (40161111, 40341300, 40261053, 39464481)
ORDER BY rmdf.CustomerAccountId

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- to compare:
SELECT * FROM [MRG].[RModelDataFlags_Pivoted_All] ORDER BY CustomerAccountId --, CustomerAccountProfileTypeCd, CustomerAccountProfileMeasureCd
GO

SELECT 
            CAPV.CustomerAccountId,
            CAPT.CustomerAccountProfileTypeCd,
            CAPM.CustomerAccountProfileMeasureCd,
            CAPV.CustomerAccountValTxt
FROM        [BusinessSemanticLayer].[dbo].CustomerAccountProfileVal CAPV
INNER JOIN  [BusinessSemanticLayer].[dbo].CustomerAccountProfileType AS CAPT ON CAPT.CustomerAccountProfileTypeId = CAPV.CustomerAccountProfileTypeId
INNER JOIN  [BusinessSemanticLayer].[dbo].CustomerAccountProfileMeasure AS CAPM ON CAPM.CustomerAccountProfileMeasureId = CAPV.CustomerAccountProfileMeasureId

WHERE CustomerAccountId = 1074
ORDER BY CustomerAccountId, CustomerAccountProfileTypeCd, CustomerAccountProfileMeasureCd
IN -- Copy and Paste below first n CustomeraccountIds FROM [BusinessSemanticLayer].[MRG].[RModelDataFlags_Pivoted_All]: 
(
1051
,1074
,1172
,1524
,1543
,1795
,1912
,2539
,2717
,2750
,2840
,2856
,2915
,3136
,3436
,3453
,3827
,3977
,4133
,4349
)
ORDER BY CustomerAccountId, CustomerAccountProfileTypeCd, CustomerAccountProfileMeasureCd
GO

/*
--EXEC [src].[sp_REF_DIP_recipientextensionsdynamicdata_105 AdobeOutbound_DFT_Load_recipientextensionsdynamicdata_DANTF_810]
--@StartDate = '2020-11-10 17:27:26.223', @EndDate = '2021-01-09 17:27:41.717'

SELECT * FROM [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data]
WHERE global_account_id IN 
(
 3485231
,3486668
,3490185
,3490297
,39464481
,40161111
,40261053
,40341300
)
*/
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$             FINAL RESULTS:          $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SELECT
            redd.*  
            , fn.*
-- 1. if we want to include records that exist BOTH in our pivot AND in recipient_extensions_dynamic_data:
--FROM [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data] redd
--CROSS APPLY [BusinessSemanticLayer].[MRG].[ufn_GetRModelDataFlags_Pivoted] (redd.[global_account_id]) fn

-- 2. if we want to include records that exist in our pivot OR (optionally) in recipient_extensions_dynamic_data:
--FROM [MRG].[RModelDataFlags_Pivoted_All] fn 
--LEFT JOIN [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data_TEMP] redd ON fn.CustomerAccountId = redd.[global_account_id]

-- 3. safest option: if we want to include records that exist recipient_extensions_dynamic_data OR (optionally) in our pivot:
FROM [StagingAdobeOutbound].[dbo].[recipient_extensions_dynamic_data] redd   
LEFT JOIN [MRG].[RModelDataFlags_Pivoted_All] fn ON fn.CustomerAccountId = redd.[global_account_id]

WHERE redd.global_account_id IN 
(
 10952
,11137
,11164
,11260
,11319
,11352
,11503
,11528
,11569
,11610
,11685
,11760
,11814
,11940
,12119
,12159
,12362
,12441
,12607
,12648
,12704
,12793
,12832
,12875
,13078
,13303
,13591
,13698
,13883
,14070
,14253

,3485231
,3486668
,3490185
,3490297
,15740784
,15742188
,15744991
,15747438
,15749402
,15758187
,15766419
,15772516
,15777490
,15786096
,15792601
,15794730
,15794826
,15797731
,15807510
,15830335
,15861041
,15866044
,15871087
,16019952
,39464481
,40161111
,40261053
,40341300
)
ORDER BY global_account_id --, CustomerAccountProfileTypeCd, CustomerAccountProfileMeasureCd
