USE [BusinessSemanticLayer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		mgrzymala
-- Create date: 2020-12-07
-- Description:	DANTF-803 - Populates RModelDataFlags_Pivoted_Tables
-- =============================================
DROP PROCEDURE IF EXISTS [MRG].[usp_Populate_RModelDataFlags_Pivoted_Tables]
GO

CREATE OR ALTER PROCEDURE [MRG].[usp_Populate_RModelDataFlags_Pivoted_Tables]
	-- Add the parameters for the stored procedure here
	@CustomerAccountProfileTypeCd VARCHAR(50), @CustomerAccountProfileMeasureCds CustomerAccountProfileMeasureCds READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET NOCOUNT ON;
DECLARE 

     @ErrMsg		     VARCHAR(MAX) 
	,@CustomErrorMessage VARCHAR(2048)
    ,@ErrorSeverity	     INT 
    ,@ErrorNumber	     INT
	,@ErrorState	     INT 
	,@ErrorLine		     INT 
	,@ErrorMessage	     VARCHAR(1000) 
    ,@@MESSAGE           VARCHAR(2048)
    ,@@PROCNAME          VARCHAR(255) 
    ,@@USERNAME          VARCHAR(32)

DECLARE @_CustomerAccountProfileMeasureCds VARCHAR(MAX)
SELECT @_CustomerAccountProfileMeasureCds = COALESCE(@_CustomerAccountProfileMeasureCds+''', ''' ,'''') + CustomerAccountProfileMeasureCd FROM @CustomerAccountProfileMeasureCds --ORDER BY CustomerAccountProfileMeasureCd
SELECT @_CustomerAccountProfileMeasureCds = @_CustomerAccountProfileMeasureCds+''''

DECLARE @_CustomerAccountProfileMeasureCds_wBrackets VARCHAR(MAX)
SELECT @_CustomerAccountProfileMeasureCds_wBrackets = COALESCE(@_CustomerAccountProfileMeasureCds_wBrackets+'], [' ,'[') + CustomerAccountProfileMeasureCd FROM @CustomerAccountProfileMeasureCds --ORDER BY CustomerAccountProfileMeasureCd
SELECT @_CustomerAccountProfileMeasureCds_wBrackets = @_CustomerAccountProfileMeasureCds_wBrackets+']'

DECLARE @sql NVARCHAR(MAX)
SET @sql = ' 
SELECT  
                   [CustomerAccountId], '+@_CustomerAccountProfileMeasureCds_wBrackets+'
FROM 
(
        SELECT 
                    CustomerAccountId,
                    CustomerAccountValTxt,
                    CustomerAccountProfileTypeCd,
                    CustomerAccountProfileMeasureCd
        FROM        MRG.RModelDataFlags 
        WHERE       1 = 1
        AND         CustomerAccountProfileTypeCd = '''+@CustomerAccountProfileTypeCd+'''
        AND         CustomerAccountProfileMeasureCd IN ('+@_CustomerAccountProfileMeasureCds+')
) p PIVOT
(
        MAX(CustomerAccountValTxt)
        FOR CustomerAccountProfileMeasureCd IN ('+@_CustomerAccountProfileMeasureCds_wBrackets+')
)       AS pvt
ORDER BY pvt.CustomerAccountId DESC
'
--PRINT @sql
BEGIN TRY

    EXEC sp_executesql @stmt = @sql

    SELECT @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')
	SELECT @@MESSAGE = 'Procedure ' + @@PROCNAME + ' executed successfully! '
	--EXEC xp_logevent 60000, @@MESSAGE, informational WITH RESULT SETS NONE; 
END TRY

BEGIN CATCH
	SET @ErrMsg = 'Error executing: '   + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown') + CHAR(13) 
	+ 'Error Procedure: '               + COALESCE(ERROR_PROCEDURE(), 'Unknown')
	+ ', Error Number: '                + CAST(ERROR_NUMBER() AS VARCHAR)
	+ ', Error Severity: '              + CAST(ERROR_SEVERITY() AS VARCHAR)
	+ ', Error State: '                 + CAST(ERROR_STATE() AS VARCHAR)
	+ ', Error Line: '                  + CAST(ERROR_LINE() AS VARCHAR)
	+ ', Error Message: '               + COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)
	+ ', Server Name: '                 + @@SERVERNAME + ' - Database: ' + DB_NAME() + ', User: ' + SUSER_NAME() + ', SPID: ' + CAST(@@SPID AS VARCHAR) + ', Now: ' + CAST(GETDATE() AS VARCHAR(121));
   SET @ErrMsg = CONCAT(@ErrMsg, ' ', @CustomErrorMessage)
   RAISERROR (@ErrMsg, @ErrorSeverity, @ErrorState);
END CATCH;
END
GO
