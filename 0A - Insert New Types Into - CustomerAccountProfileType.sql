USE [BusinessSemanticLayer]
GO

DELETE FROM CustomerAccountProfileType WHERE CustomerAccountProfileTypeNm IN (
 'CVMGamingRollingNew'
,'CVMSportsbookRollingNew'
,'GamingChurnPrevention'
)

INSERT INTO dbo.CustomerAccountProfileType
(
    CustomerAccountProfileTypeId,
    CustomerAccountProfileTypeCd,
    CustomerAccountProfileTypeNm,
    CustomerAccountProfileTypeDsc,
    InformationSourceId,
    SystemCreatedDt,
    UpdateByStaffId,
    SystemUpdatedDt,
    LowWaterMark,
    HighWaterMark,
    ProfileOwner,
    RightToBeForgottenExclusionFlag
)
VALUES
(104118, 'CVMGamingRollingNew', 'CVMGamingRollingNew', '', 1, GETDATE(), 1, GETDATE(), NULL, NULL, '', ''),
(104119, 'CVMSportsbookRollingNew', 'CVMSportsbookRollingNew', '', 1, GETDATE(), 1, GETDATE(), NULL, NULL, '', ''),
(104120, 'GamingChurnPrevention', 'GamingChurnPrevention', '', 1, GETDATE(), 1, GETDATE(), NULL, NULL, '', '')
GO

-- to verify:
SELECT * FROM CustomerAccountProfileType WHERE CustomerAccountProfileTypeNm IN (
 'CVMGamingRollingNew'
,'CVMSportsbookRollingNew'
,'GamingChurnPrevention'
)
--SELECT MAX(CustomerAccountProfileTypeId) FROM CustomerAccountProfileType

SELECT DISTINCT capt.CustomerAccountProfileTypeId,
                rmf.CustomerAccountProfileTypeCd
FROM            MRG.RModelDataFlags rmf
INNER JOIN      dbo.CustomerAccountProfileType AS capt ON capt.CustomerAccountProfileTypeCd = rmf.CustomerAccountProfileTypeCd
ORDER BY        rmf.CustomerAccountProfileTypeCd
/*
104118	CVMGamingRollingNew
104119	CVMSportsbookRollingNew
104120	GamingChurnPrevention
*/

