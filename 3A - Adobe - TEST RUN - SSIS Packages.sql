USE [SSISDB]
GO

-- 1.connect to SSIS Services Instance
-- 2.execute both packages in the order below:
/*
DECLARE @execution_id BIGINT
EXEC [SSISDB].[catalog].[create_execution] @package_name=N'REF_DIP_recipientextensionsdynamicdata_105 AdobeOutbound.dtsx', @execution_id=@execution_id OUTPUT, @folder_name=N'DataManagementPlatform', @project_name=N'BSL_SAM_Integration', @use32bitruntime=False, @reference_id=57
SELECT @execution_id
DECLARE @var0 SMALLINT = 1
EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'LOGGING_LEVEL', @parameter_value=@var0
EXEC [SSISDB].[catalog].[set_execution_property_override_value] @execution_id,  @property_path=N'\Package.Variables[User::gv_CurrReqId].Value', @property_value=N'905', @sensitive=False
EXEC [SSISDB].[catalog].[start_execution] @execution_id
GO

DECLARE @execution_id BIGINT
EXEC [SSISDB].[catalog].[create_execution] @package_name=N'REF_DDF_recipientextensionsdynamicdata_115_AdobeOutbound.dtsx', @execution_id=@execution_id OUTPUT, @folder_name=N'DataManagementPlatform', @project_name=N'SAM_STG_DataDelivery', @use32bitruntime=False, @reference_id=59
SELECT @execution_id
DECLARE @var0 SMALLINT = 1
EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'LOGGING_LEVEL', @parameter_value=@var0
EXEC [SSISDB].[catalog].[start_execution] @execution_id
GO
*/
-- 3.verify package execution:
USE SSISDB
GO

DECLARE @DateSince DATE = GETDATE() - 30

SELECT
    ei.execution_id,
    ei.folder_name,
    ei.project_name,
    ei.package_name,
    CAST(ei.start_time AS DATETIME2(0))             AS [StartTime],
    CAST(ei.end_time AS DATETIME2(0))               AS [EndTime],
    CASE ei.status 
          WHEN 1 THEN 'Created'
          WHEN 2 THEN 'Running'
          WHEN 3 THEN 'Cancelled'
          WHEN 4 THEN 'Failed'
          WHEN 5 THEN 'About to run'
          WHEN 6 THEN 'Ended unexpectedly'
          WHEN 7 THEN 'Success'
          WHEN 8 THEN 'Stopping'
          WHEN 9 THEN 'Completed'
          ELSE 'Unknown'
    END AS [Status],
    DATEDIFF(SECOND, ei.start_time, ei.end_time)    AS [execution_time (sec)],
    DATEDIFF(MINUTE, ei.start_time, ei.end_time)    AS [execution_time (min)],
    ei.environment_folder_name,
    ei.environment_name,
    ei.executed_as_name,
    ov.property_path,
    ov.property_value,
    ei.use32bitruntime,
    ei.operation_type,
    CAST(ei.created_time AS DATETIME2(0))             AS [CreatedTime],
    ei.object_type

FROM           [SSISDB].internal.execution_info ei WITH (NOLOCK)
LEFT JOIN      [SSISDB].catalog.execution_property_override_values ov WITH (NOLOCK)  ON ov.execution_id = ei.execution_id

WHERE
        ei.package_name --= @PackageName AND
        
        IN (
              -- Adobe:
              'REF_DIP_recipientextensionsdynamicdata_105 AdobeOutbound.dtsx'
              ,'REF_DDF_recipientextensionsdynamicdata_115_AdobeOutbound.dtsx'
              
              -- Tealium:
              ,'REF_DIP_RecipientExtensionsDynamicData_BSL TL002.dtsx'
              ,'REF_DIP_CombineData_BSL TL005.dtsx'
              ,'REF_DDF_BlacklistFlags_TL_OUT001.dtsx'
        ) AND 
            
        ei.start_time >= @DateSince

ORDER BY ei.start_time DESC

-- 4. on OPS Server check path 'C:\ClusterStorage\Acquisition\DataPlatform\Output' for new file 'recipient_extensions_dynamic_data_YYYYMMDD_HHmmSS.csv'
