USE [BusinessSemanticLayer]
GO

DECLARE @CustomerAccountId INT = 4203456

-- CustomerAccountProfileVal entries:
SELECT 
            CAPV.CustomerAccountId,
            CAPT.CustomerAccountProfileTypeCd,
            CAPM.CustomerAccountProfileMeasureCd,
            CAPV.CustomerAccountValTxt
FROM        [BusinessSemanticLayer].[dbo].CustomerAccountProfileVal CAPV
INNER JOIN  [BusinessSemanticLayer].[dbo].CustomerAccountProfileType AS CAPT ON CAPT.CustomerAccountProfileTypeId = CAPV.CustomerAccountProfileTypeId
INNER JOIN  [BusinessSemanticLayer].[dbo].CustomerAccountProfileMeasure AS CAPM ON CAPM.CustomerAccountProfileMeasureId = CAPV.CustomerAccountProfileMeasureId
WHERE       CustomerAccountId = @CustomerAccountId

ORDER BY CustomerAccountId, CustomerAccountProfileTypeCd, CustomerAccountProfileMeasureCd

-- compare with Pivot:
SELECT   CustomerAccountId,

         --CVMGamingRollingNew_PredictedChurnPreventionScore,
         --CVMGamingRollingNew_PredictedGWPercGivenActive,
         CVMGamingRollingNew_PredictedProbabilityActive,
         CVMGamingRollingNew_PredictedProbabilityCustomerWins,
         CVMGamingRollingNew_PredictedNGRGivenActive,


         --CVMGamingRollingNew_PredictedStakeGivenActive,
         
         --CVMSportsbookRollingNew_PredictedChurnPreventionScore,
         CVMSportsbookRollingNew_PredictedProbabilityActive,
         CVMSportsbookRollingNew_PredictedGWPercGivenActive,
         --CVMSportsbookRollingNew_PredictedNGRGivenActive,
         CVMSportsbookRollingNew_PredictedProbabilityCustomerWins,
         CVMSportsbookRollingNew_PredictedStakeGivenActive,
         
         GamingChurnPrevention_PredictedChurnPreventionScore
         --GamingChurnPrevention_PredictedGWPercGivenActive,
         --GamingChurnPrevention_PredictedNGRGivenActive,
         --GamingChurnPrevention_PredictedProbabilityActive,
         --GamingChurnPrevention_PredictedProbabilityCustomerWins,
         --GamingChurnPrevention_PredictedStakeGivenActive 

FROM     [BusinessSemanticLayer].[MRG].[RModelDataFlags_Pivoted_All]
WHERE    CustomerAccountId = @CustomerAccountId