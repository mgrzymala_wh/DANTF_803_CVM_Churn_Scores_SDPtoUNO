USE [StagingTeliumOutbound]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================================================================================  
-- Author:  WHGROUP\mgrzymala    
-- Create date: 2020-12-09   
-- Description: Create  [src].[usp_Populate_recipientextensionsdynamicdata_EXTND]  
-- =======================================================================================================================================
/*
--to run:
TRUNCATE TABLE [StagingTeliumOutbound].[dbo].[recipientextensionsdynamicdata_EXTND]
EXEC [StagingTeliumOutbound].[dbo].[usp_Populate_recipientextensionsdynamicdata_EXTND]
--to test:
SELECT * FROM [StagingTeliumOutbound].[dbo].[recipientextensionsdynamicdata_EXTND] ORDER BY CustomerAccountId DESC
*/
-- =======================================================================================================================================  
-- Change History   
-- Date         User						Change                   
-- 2020-12-09   mgrzymala                   DANTF-811 - initial version
-- =======================================================================================================================================  

CREATE OR ALTER PROCEDURE [dbo].[usp_Populate_recipientextensionsdynamicdata_EXTND]
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
SET NOCOUNT ON;
SET XACT_ABORT ON;

TRUNCATE TABLE [StagingTeliumOutbound].[dbo].[recipientextensionsdynamicdata_EXTND]
INSERT INTO [StagingTeliumOutbound].[dbo].[recipientextensionsdynamicdata_EXTND] (
        CustomerAccountId,
        risk_profile_id,
        viplevel_casino_cd,
        viplevel_poker_cd,
        viplevel_vegas_cd,
        account_balance_GBP,
        last_login_date,
        last_system_cd,
        last_product_cd,
        last_withdrawal_date,
        last_deposit_date,
        last_win_date,
        last_win_amount_gbp,
        last_bonus_date,
        last_withdrawal_amount_gbp,
        last_bonus_amount_gbp,
        bingo_bonus_balance,
        bingo_loyalty_points_balance,
        player_protection_model_flag,
        EligibilityFlag,
        EligibilityDate,
        OptionInFlag,
        OptInDate,
        MaximumStakefactor,
        InPlayFlag,
        MobileFlag,
        bingo_pending_balance,
        bingo_loyalty_level,
        bingo_date_level_achieved,
        bingo_current_period_loyalty_points,
        current_balance_complimentary_points,
        PokerFirstActivityDt,
        CasinoFirstActivityDt,
        VegasFirstActivityDt,
        GamesFirstActivityDt,
        LiveCasinoFirstActivityDt,
        PokerSideGamesFirstActivityDt,
        ScratchCardsGamesFirstActivityDt,
        BetmostPokerFirstActivityDt,
        BiGSLiCKPokerFirstActivityDt,
        CDPokerFirstActivityDt,
        SportsFirstActivityDt,
        BingoFirstActivityDt,
        tot_num_of_deposits,
        total_lifetime_value_deposits_gbp,
        total_number_of_bets,
        total_lifetime_bet_value_gbp,
        total_active_days,
        total_lifetime_value_withdrawals_gbp,
        bingo_cust_acct_activity_status_cd,
        casino_cust_acct_activity_status_cd,
        live_casino_cust_acct_activity_status_cd,
        poker_cust_acct_activity_status_cd,
        scratchcard_cust_acct_activity_status_cd,
        sportsbook_cust_acct_activity_status_cd,
        vegas_cust_acct_activity_status_cd,
        games_cust_acct_activity_status_cd,
        activity_type_cd,
        priority_second_product_cd,
        priority_third_product_cd,
        priority_fourth_product_cd,
        priority_fifth_product_cd,
        priority_sixth_product_cd,
        priority_second_sub_product_cd,
        priority_third_sub_product_cd,
        priority_fourth_sub_product_cd,
        priority_fifth_sub_product_cd,
        priority_sixth_sub_product_cd,
        poker_reactivation_status,
        bingo_cust_acct_activity_Type_cd,
        casino_cust_acct_activity_Type_cd,
        live_casino_cust_acct_activity_Type_cd,
        poker_cust_acct_activity_Type_cd,
        scratchcard_cust_acct_activity_Type_cd,
        sportsbook_cust_acct_activity_Type_cd,
        vegas_cust_acct_activity_Type_cd,
        games_cust_acct_activity_Type_cd,
        last_bet_amount_GBP,
        bingosidegames_cust_acct_activity_type_cd,
        bingosidegames_cust_acct_activity_status_cd,
        BingoSideGamesFirstActivityDt,
        EuroExchangeRate,
        KroneExchangeRate,
        DollarExchangeRate,
        YenExchangeRate,
        TotalLifetimeActiveDaysOmni,
        TotalLifetimeBetsGBPOmni,
        TotalLifetimeDepositsGBPOmni,
        TotalLifetimeWithdrawalsGBPOmni,
        PriorityFirstChannelCd,
        PrioritySecondChannelCd,
        PriorityThirdChannelCd,
        GraduationDt,
        SportsFirstOmniActivityDt,
        VegasFirstOmniActivityDt,
        CasinoFirstOmniActivityDt,
        LiveCasinoFirstOmniActivityDt,
        GamesFirstOmniActivityDt,
        last_ssbt_bet_date,
        CustomerAccountOmniStatusCd,
        CustomerAccountOmniLinkDt,
        next_card_expiry_dt,
        reactivation_status_sportsbook,
        reactivation_status_vegas,
        reactivation_status_macau,
        reactivation_status_casino,
        reactivation_status_live_casino,
        reactivation_status_games,
        reactivation_status_scratchcards,
        reactivation_status_bingo,
        reactivation_status_poker,
        customer_second_lifestage_cd,
        churn_flag,
        churn_gaming_value,
        customer_value_model_unified,
        customer_value_model_sportsbook,
        customer_value_model_gaming,
        RequestId,
        SourceSystemId,
        SystemCreatedDt,
        -----------------------------------------------------------
        CVMGamingRollingNew_PredictedChurnPreventionScore,
        CVMGamingRollingNew_PredictedGWPercGivenActive,
        CVMGamingRollingNew_PredictedNGRGivenActive,
        CVMGamingRollingNew_PredictedProbabilityActive,
        CVMGamingRollingNew_PredictedProbabilityCustomerWins,
        CVMGamingRollingNew_PredictedStakeGivenActive,
        CVMSportsbookRollingNew_PredictedChurnPreventionScore,
        CVMSportsbookRollingNew_PredictedGWPercGivenActive,
        CVMSportsbookRollingNew_PredictedNGRGivenActive,
        CVMSportsbookRollingNew_PredictedProbabilityActive,
        CVMSportsbookRollingNew_PredictedProbabilityCustomerWins,
        CVMSportsbookRollingNew_PredictedStakeGivenActive,
        GamingChurnPrevention_PredictedChurnPreventionScore,
        GamingChurnPrevention_PredictedGWPercGivenActive,
        GamingChurnPrevention_PredictedNGRGivenActive,
        GamingChurnPrevention_PredictedProbabilityActive,
        GamingChurnPrevention_PredictedProbabilityCustomerWins,
        GamingChurnPrevention_PredictedStakeGivenActive

        --[cvm_predicted_ngr_gaming],  -- this is a computed column, we do not want to insert into it
        --[cvm_prodicted_ngr_sportsbook], -- this is a computed column, we do not want to insert into it  
)

SELECT 
        redd.CustomerAccountId,
        redd.risk_profile_id,
        redd.viplevel_casino_cd,
        redd.viplevel_poker_cd,
        redd.viplevel_vegas_cd,
        redd.account_balance_GBP,
        redd.last_login_date,
        redd.last_system_cd,
        redd.last_product_cd,
        redd.last_withdrawal_date,
        redd.last_deposit_date,
        redd.last_win_date,
        redd.last_win_amount_gbp,
        redd.last_bonus_date,
        redd.last_withdrawal_amount_gbp,
        redd.last_bonus_amount_gbp,
        redd.bingo_bonus_balance,
        redd.bingo_loyalty_points_balance,
        redd.player_protection_model_flag,
        redd.EligibilityFlag,
        redd.EligibilityDate,
        redd.OptionInFlag,
        redd.OptInDate,
        redd.MaximumStakefactor,
        redd.InPlayFlag,
        redd.MobileFlag,
        redd.bingo_pending_balance,
        redd.bingo_loyalty_level,
        redd.bingo_date_level_achieved,
        redd.bingo_current_period_loyalty_points,
        redd.current_balance_complimentary_points,
        redd.PokerFirstActivityDt,
        redd.CasinoFirstActivityDt,
        redd.VegasFirstActivityDt,
        redd.GamesFirstActivityDt,
        redd.LiveCasinoFirstActivityDt,
        redd.PokerSideGamesFirstActivityDt,
        redd.ScratchCardsGamesFirstActivityDt,
        redd.BetmostPokerFirstActivityDt,
        redd.BiGSLiCKPokerFirstActivityDt,
        redd.CDPokerFirstActivityDt,
        redd.SportsFirstActivityDt,
        redd.BingoFirstActivityDt,
        redd.tot_num_of_deposits,
        redd.total_lifetime_value_deposits_gbp,
        redd.total_number_of_bets,
        redd.total_lifetime_bet_value_gbp,
        redd.total_active_days,
        redd.total_lifetime_value_withdrawals_gbp,
        redd.bingo_cust_acct_activity_status_cd,
        redd.casino_cust_acct_activity_status_cd,
        redd.live_casino_cust_acct_activity_status_cd,
        redd.poker_cust_acct_activity_status_cd,
        redd.scratchcard_cust_acct_activity_status_cd,
        redd.sportsbook_cust_acct_activity_status_cd,
        redd.vegas_cust_acct_activity_status_cd,
        redd.games_cust_acct_activity_status_cd,
        redd.activity_type_cd,
        redd.priority_second_product_cd,
        redd.priority_third_product_cd,
        redd.priority_fourth_product_cd,
        redd.priority_fifth_product_cd,
        redd.priority_sixth_product_cd,
        redd.priority_second_sub_product_cd,
        redd.priority_third_sub_product_cd,
        redd.priority_fourth_sub_product_cd,
        redd.priority_fifth_sub_product_cd,
        redd.priority_sixth_sub_product_cd,
        redd.poker_reactivation_status,
        redd.bingo_cust_acct_activity_Type_cd,
        redd.casino_cust_acct_activity_Type_cd,
        redd.live_casino_cust_acct_activity_Type_cd,
        redd.poker_cust_acct_activity_Type_cd,
        redd.scratchcard_cust_acct_activity_Type_cd,
        redd.sportsbook_cust_acct_activity_Type_cd,
        redd.vegas_cust_acct_activity_Type_cd,
        redd.games_cust_acct_activity_Type_cd,
        redd.last_bet_amount_GBP,
        redd.bingosidegames_cust_acct_activity_type_cd,
        redd.bingosidegames_cust_acct_activity_status_cd,
        redd.BingoSideGamesFirstActivityDt,
        redd.EuroExchangeRate,
        redd.KroneExchangeRate,
        redd.DollarExchangeRate,
        redd.YenExchangeRate,
        redd.TotalLifetimeActiveDaysOmni,
        redd.TotalLifetimeBetsGBPOmni,
        redd.TotalLifetimeDepositsGBPOmni,
        redd.TotalLifetimeWithdrawalsGBPOmni,
        redd.PriorityFirstChannelCd,
        redd.PrioritySecondChannelCd,
        redd.PriorityThirdChannelCd,
        redd.GraduationDt,
        redd.SportsFirstOmniActivityDt,
        redd.VegasFirstOmniActivityDt,
        redd.CasinoFirstOmniActivityDt,
        redd.LiveCasinoFirstOmniActivityDt,
        redd.GamesFirstOmniActivityDt,
        redd.last_ssbt_bet_date,
        redd.CustomerAccountOmniStatusCd,
        redd.CustomerAccountOmniLinkDt,
        redd.next_card_expiry_dt,
        redd.reactivation_status_sportsbook,
        redd.reactivation_status_vegas,
        redd.reactivation_status_macau,
        redd.reactivation_status_casino,
        redd.reactivation_status_live_casino,
        redd.reactivation_status_games,
        redd.reactivation_status_scratchcards,
        redd.reactivation_status_bingo,
        redd.reactivation_status_poker,
        redd.customer_second_lifestage_cd,
        redd.churn_flag,
        redd.churn_gaming_value,
        redd.customer_value_model_unified,
        redd.customer_value_model_sportsbook,
        redd.customer_value_model_gaming,
        redd.RequestId,
        redd.SourceSystemId,
        redd.SystemCreatedDt,
        -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
              fn.CVMGamingRollingNew_PredictedChurnPreventionScore,
              fn.CVMGamingRollingNew_PredictedGWPercGivenActive,
              fn.CVMGamingRollingNew_PredictedNGRGivenActive,
              fn.CVMGamingRollingNew_PredictedProbabilityActive,
              fn.CVMGamingRollingNew_PredictedProbabilityCustomerWins,
              fn.CVMGamingRollingNew_PredictedStakeGivenActive,
              fn.CVMSportsbookRollingNew_PredictedChurnPreventionScore,
              fn.CVMSportsbookRollingNew_PredictedGWPercGivenActive,
              fn.CVMSportsbookRollingNew_PredictedNGRGivenActive,
              fn.CVMSportsbookRollingNew_PredictedProbabilityActive,
              fn.CVMSportsbookRollingNew_PredictedProbabilityCustomerWins,
              fn.CVMSportsbookRollingNew_PredictedStakeGivenActive,
              fn.GamingChurnPrevention_PredictedChurnPreventionScore,
              fn.GamingChurnPrevention_PredictedGWPercGivenActive,
              fn.GamingChurnPrevention_PredictedNGRGivenActive,
              fn.GamingChurnPrevention_PredictedProbabilityActive,
              fn.GamingChurnPrevention_PredictedProbabilityCustomerWins,
              fn.GamingChurnPrevention_PredictedStakeGivenActive

              --NULL AS [cvm_predicted_ngr_gaming], -- this is a computed column, we do not want to insert into it    
              --NULL AS [cvm_prodicted_ngr_sportsbook], -- this is a computed column, we do not want to insert into it 

-- 1. if we want to include records that exist BOTH in our pivot AND in recipientextensionsdynamicdata:
--FROM [StagingAdobeOutbound].[dbo].[recipientextensionsdynamicdata] redd
--CROSS APPLY [BusinessSemanticLayer].[MRG].[ufn_GetRModelDataFlags_Pivoted] (redd.[CustomerAccountId]) fn

-- 2. if we want to include records that exist in our pivot OR (optionally) in recipientextensionsdynamicdata:
--FROM [MRG].[RModelDataFlags_Pivoted_All] fn 
--LEFT JOIN [StagingAdobeOutbound].[dbo].[recipientextensionsdynamicdata] redd ON fn.CustomerAccountId = redd.[CustomerAccountId]

-- 3. safest option: if we want to include all records that exist recipientextensionsdynamicdata OR exist (optionally) in our pivot table:
FROM [StagingTeliumOutbound].[dbo].[recipientextensionsdynamicdata] redd   
LEFT JOIN [BusinessSemanticLayer].[MRG].[RModelDataFlags_Pivoted_All] fn ON fn.[CustomerAccountId] = redd.[CustomerAccountId]
END
