USE [StagingTeliumOutbound]
GO

/*
https://jira.willhillatlas.com/browse/DANTF-811:
CVMGamingRollingNew: 
CVMGamingRollingPredictedProbabilityActive * CVMGamingRollingNGRGivenActive = cvm_predicted_ngr_gaming
*/

CREATE OR ALTER FUNCTION [dbo].[ufn_Get_cvm_predicted_ngr_gaming] (@CustomerAccountIdID AS int)
RETURNS TABLE
AS
  RETURN
  (
        SELECT 
          redd.CustomerAccountId,
          CAST(
              CAST( 
                    (
                      CAST(redd.CVMGamingRollingNew_PredictedProbabilityActive AS DECIMAL (20, 2)) 
                    * CAST(redd.CVMGamingRollingNew_PredictedNGRGivenActive AS DECIMAL (20, 2))
                    ) AS DECIMAL (20, 4)
                  ) AS VARCHAR(50)
              ) AS [cvm_predicted_ngr_gaming]

        FROM [dbo].[recipientextensionsdynamicdata_EXTND] redd WHERE redd.CustomerAccountId = @CustomerAccountIdID
  )
GO



