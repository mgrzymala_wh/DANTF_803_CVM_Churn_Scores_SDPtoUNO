USE [StagingTeliumOutbound]
GO

ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMGamingRollingNew_PredictedChurnPreventionScore          VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMGamingRollingNew_PredictedGWPercGivenActive             VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMGamingRollingNew_PredictedNGRGivenActive                VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMGamingRollingNew_PredictedProbabilityActive             VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMGamingRollingNew_PredictedProbabilityCustomerWins       VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMGamingRollingNew_PredictedStakeGivenActive              VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMSportsbookRollingNew_PredictedChurnPreventionScore      VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMSportsbookRollingNew_PredictedGWPercGivenActive         VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMSportsbookRollingNew_PredictedNGRGivenActive            VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMSportsbookRollingNew_PredictedProbabilityActive         VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMSportsbookRollingNew_PredictedProbabilityCustomerWins   VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD CVMSportsbookRollingNew_PredictedStakeGivenActive          VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD GamingChurnPrevention_PredictedChurnPreventionScore        VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD GamingChurnPrevention_PredictedGWPercGivenActive           VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD GamingChurnPrevention_PredictedNGRGivenActive              VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD GamingChurnPrevention_PredictedProbabilityActive           VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD GamingChurnPrevention_PredictedProbabilityCustomerWins     VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD GamingChurnPrevention_PredictedStakeGivenActive            VARCHAR(50)
ALTER TABLE [dbo].[ExportDailyCustomers] ADD [cvm_predicted_ngr_gaming]                                 FLOAT                                                
ALTER TABLE [dbo].[ExportDailyCustomers] ADD [cvm_predicted_ngr_sportsbook]                             FLOAT                                


ALTER TABLE [dbo].[ExportDailyCustomers] ADD [cvm_predicted_overall_ngr]                                AS CAST
                                       (
                                          ([cvm_predicted_ngr_gaming] + [cvm_predicted_ngr_sportsbook]) AS FLOAT
                                       )


ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMGamingRollingNew_PredictedChurnPreventionScore]       
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMGamingRollingNew_PredictedGWPercGivenActive]          
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMGamingRollingNew_PredictedNGRGivenActive]             
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMGamingRollingNew_PredictedProbabilityActive]          
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMGamingRollingNew_PredictedProbabilityCustomerWins]    
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMGamingRollingNew_PredictedStakeGivenActive]           
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMSportsbookRollingNew_PredictedChurnPreventionScore]   
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMSportsbookRollingNew_PredictedGWPercGivenActive]      
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMSportsbookRollingNew_PredictedNGRGivenActive]         
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMSportsbookRollingNew_PredictedProbabilityActive]      
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMSportsbookRollingNew_PredictedProbabilityCustomerWins]
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [CVMSportsbookRollingNew_PredictedStakeGivenActive]       
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [GamingChurnPrevention_PredictedChurnPreventionScore]     
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [GamingChurnPrevention_PredictedGWPercGivenActive]        
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [GamingChurnPrevention_PredictedNGRGivenActive]           
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [GamingChurnPrevention_PredictedProbabilityActive]        
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [GamingChurnPrevention_PredictedProbabilityCustomerWins]  
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [GamingChurnPrevention_PredictedStakeGivenActive]         

ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [cvm_prodicted_overall_ngr]
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [cvm_predicted_ngr_gaming]
ALTER TABLE [dbo].[ExportDailyCustomers] DROP COLUMN [cvm_prodicted_ngr_sportsbook]
                               


/*
SELECT CustomerAccountId
       ,CVMGamingRollingNew_PredictedChurnPreventionScore       
       ,CVMGamingRollingNew_PredictedGWPercGivenActive          
       ,CVMGamingRollingNew_PredictedNGRGivenActive             
       ,CVMGamingRollingNew_PredictedProbabilityActive          
       ,CVMGamingRollingNew_PredictedProbabilityCustomerWins    
       ,CVMGamingRollingNew_PredictedStakeGivenActive           
       ,CVMSportsbookRollingNew_PredictedChurnPreventionScore   
       ,CVMSportsbookRollingNew_PredictedGWPercGivenActive      
       ,CVMSportsbookRollingNew_PredictedNGRGivenActive         
       ,CVMSportsbookRollingNew_PredictedProbabilityActive      
       ,CVMSportsbookRollingNew_PredictedProbabilityCustomerWins
       ,CVMSportsbookRollingNew_PredictedStakeGivenActive       
       ,GamingChurnPrevention_PredictedChurnPreventionScore     
       ,GamingChurnPrevention_PredictedGWPercGivenActive        
       ,GamingChurnPrevention_PredictedNGRGivenActive           
       ,GamingChurnPrevention_PredictedProbabilityActive        
       ,GamingChurnPrevention_PredictedProbabilityCustomerWins  
       ,GamingChurnPrevention_PredictedStakeGivenActive         

FROM [dbo].[ExportDailyCustomers]
ORDER BY CustomerAccountId, CustomerModelValueGamesScore

*/